---
title: 观看代码小憩
video_player: true
theme: responsive
social:
  og:title: Code.org Code Break
  og:description: Learn CS with Hadi Partovi and the team at Code.org with a live weekly webcast and weekly challenges for students of all abilities
  og:image: http://code.org/images/marketing/announcement_special2020.jpg
  og:image:width: 850
  og:image:height: 502
  og:video: ''
  og:video:width: ''
  og:video:height: ''
  og:video:type: ''
---

<div style="background-color: var(--brand_primary_default); padding: 15px; margin-top:50px;"><img src="/images/athome/CodeBreak-horizontal.png" style="max-width: 100%"/></div>
<h1>观看代码小憩</h1>
<div class="col-66" style="padding-right: 28px; padding-bottom: 20px;">

<p>代码小憩是计算机科学与技术更有趣！ 有着十多<a href="#activities">剧集和活动</a>可供选择，你可以创作一个由阿什顿·库彻领导的音乐会，与 Yara Shahidi 一起学习变量，甚至可以与互联网的创始人文顿·瑟夫学习互联网是如何运行的</p>

<p>每一集都是由Code.org的创始者，<a href="https://code.org/about/leadership/hadi_partovi">哈迪帕托维</a>，并有其他启发性的客人，计算机科学理论的迷你课，和一个每周性的挑战使各年龄段和能力的学生互动，即使没有电脑也能参与。</p>

<a href="#activities"><button style="background-color: #ffa400;border-color: #ffa400;color: #FFFFFF;font-size: 16px;">看剧集</button></a>

<p><br>虽然我们的节目直播暂时结束了，您还是可以观看过往剧集并完成相关的活动。 为了增加趣味，您也可以尝试我们在<a href="https://code.org/athome">在家学习</a>中所推荐的项目和活动。</p>
<br>

<div style="max-width:490px; position:relative;">{{ codebreak_promoreel }}</div>
<p>在<a href="http://facebook.com/code.org">脸书</a>或<a href="http://twitter.com/codeorg">推特</a>上关注我们。</p>
</div>

<div class="col-33" style=" border: solid 1px #bbb; border-radius: 5px; overflow: hidden; position: relative; float: left; box-sizing: border-box;">
<img src="/images/marketing/code-break-callout-episode-0.jpg" style="max-width:100%">
<a id="signup"></a>
<div style="padding:8px 18px 0 8px;">
<p style="margin: 10px 0 0 0;">保持连接！ 添加您的地址到我们的邮件列表以接收在家学习和Code.org的更新。</p>
<iframe src="http://go.pardot.com/l/153401/2020-03-16/n7j6zc" width="100%" height="305" type="text/html" frameborder="0" allowTransparency="true" style="overflow:hidden"></iframe>

</div></div>

<div style="clear: both;"></div>
<div style="max-width:100%;">
<h2>有趣和基础性的</h2>
<p>计算机科学<i>很有趣</i>的原因是这是一个很有创意和好玩的学科。 无论是解决难题，编程艺术，或设计软件，学生偏向于探索而不是记忆。 学生们甚至将计算机科学归于他们<a href="https://code.org/promote">最喜欢的学科</a>，排名在艺术，音乐和舞蹈后。</p>
<p>计算机科学是各个学科的<a href="/promote">基础</a>，但是很多学校还暂时不提供本学科，这可以成为一个奇特的支持你孩子的有趣的学习机会。 并且，<a href="https://medium.com/@codeorg/cs-helps-students-outperform-in-school-college-and-workplace-66dd64a69536">演技表明</a>：学习计算机科学的学生在其它科目表现得更好，出众的问题解决能力，并且<font color="00adbc"><b>17%更有可能参加大学</font></b>。</p></div>

<div style="clear: both;"></div>
<a id="activities"></a>

<div style="background-color: var(--brand_secondary_default); margin-top:50px;"><h1 style="color: #FFFFFF; padding-top: 15px; padding-bottom: 15px; padding-left: 10px; padding-right: 10px;">每周活动&amp;过往剧集</h1></div>
<h2>第一集-算法与 Hill Harper。</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看3/25日的剧集</h4>
<a href="https://www.youtube.com/watch?v=IiHveVqxvcw"><img src="/images/marketing/episode-1-hill-harper.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br> <a href="https://www.youtube.com/watch?v=IiHveVqxvcw"><i>在YouTube打开</a></i>或<i><a href="https://www.facebook.com/Code.org/videos/212168439992679/">在脸书打开</i></a></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动：算法</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="http://studio.code.org/s/code-break-younger/lessons/1/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="http://studio.code.org/s/code-break/lessons/1/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<div style="clear: both;"></div>
<h2>第2集 - 与马克·库班和林德·斯科特一起探讨原型机</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看4/1日的剧集</h4>
<a href="https://www.youtube.com/watch?v=guNM-3CXgnQ"><img src="/images/marketing/code-break-episode2-screenshot.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=guNM-3CXgnQ"><i>在YouTube中打开</i></a>|<a href="https://www.facebook.com/Code.org/videos/231140824935457/"><i>在脸书打开</i></a></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动 - 原型机</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/2/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/2/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<a id="episode3"></a>
<div style="clear: both;"></div>
<h2>第3集 - 与阿什顿·库彻和米娅·吉尔·埃普纳一起探讨加密技术</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看4/8日的剧集</h4>
<a href="https://youtu.be/_RrmereHcTQ"><img src="/images/marketing/code-break-april-8-video-final.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://youtu.be/_RrmereHcTQ"><i>在YouTube打开</i></a>|在<a href="https://www.facebook.com/Code.org/videos/221983958869058/"><i>在脸书打开</i></a></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周练习-加密法</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/3/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/3/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<div style="clear: both;"></div>

<a id="episode4"></a>
<div style="clear: both;"></div>
<h2>第4集 - 与迈克·克里格和爱丽丝·基勒一起探讨数字信息</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看4/15日的剧集</h4>
<a href="https://www.youtube.com/watch?v=kL-3o5EiLsM"><img src="/images/marketing/code-break-april-15-video.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=kL-3o5EiLsM"><i>在Youtube观看视频</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/1642489922621088/ ">在Facebook观看视频</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动 - 数字信息</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/4/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/4/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<div style="clear: both;"></div>

<a id="episode5"></a>
<div style="clear: both;"></div>
<h2>第5集 - 与比尔·盖茨一起探讨模拟和数据</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看第4/22集视频</h4>
<a href="https://www.youtube.com/watch?v=fv5g9xw_jek"><img src="/images/marketing/code-break-bill-gates.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=fv5g9xw_jek"><i>在Youtube观看视频</i></a> I <i><a href="https://www.facebook.com/watch/live/?v=870396600091039&ref=watch_permalink">在Facebook观看视频</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动 - 模拟和数据</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/5/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/5/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<div style="clear: both;"></div>

<a id="episode6"></a>
<div style="clear: both;"></div>
<h2>第6集 - 与基根-迈克尔·奇和温特·瑟夫一起探讨互联网</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看第4/29集视频</h4>
<a href="https://www.youtube.com/watch?v=7jcnJR6aZfs"><img src="/images/marketing/code-break-episode-6.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=7jcnJR6aZfs"><i>在Youtube观看视频</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/267326194655266/">在Facebook观看视频</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动 - 互联网</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/6/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/6/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<div style="clear: both;"></div>

<a id="episode7"></a>
<div style="clear: both;"></div>
<h2>第7集 - 与萨尔·可汗和弗洛·沃恩一起探讨条件语句</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看第5/6集视频</h4>
<a href="https://www.youtube.com/watch?v=oKwvRwmvXsE"><img src="/images/marketing/episode-7-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=oKwvRwmvXsE"><i>在Youtube观看视频</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/838885113258946/">在Facebook观看视频</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动 - 条件语句</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/7/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/7/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<a id="episode8"></a>
<div style="clear: both;"></div>
<h2>第8集 - 与亚拉·沙希迪和法尔扎德·科斯罗萨西一起探讨变量</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看第5/13集视频</h4>
<a href="https://www.youtube.com/watch?v=bQElba6x6yI"><img src="/images/marketing/episode-8-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=bQElba6x6yI"><i>在Youtube观看视频</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/580781632554625/">在Facebook观看视频</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动 - 变量</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/8/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/8/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<a id="episode9"></a>
<div style="clear: both;"></div>
<h2>第9集 - 与麦可莫和斯科特·福斯特尔一起探讨事件</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看第5/20集视频</h4>
<a href="https://www.youtube.com/watch?v=-bcO-X9thds"><img src="/images/marketing/episode-9-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=-bcO-X9thds"><i>在Youtube观看视频</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/3261824343882823/">在Facebook观看视频</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动 - 事件</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/9/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/9/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<a id="episode10"></a>
<div style="clear: both;"></div>
<h2>第10集 - 与卡特琳娜·格兰厄姆和凯特·帕克一起探讨人工智能</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看第5/27集视频</h4>
<a href="https://www.youtube.com/watch?v=MLH2bKpXkm8"><img src="/images/marketing/episode-10-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=MLH2bKpXkm8"><i>在Youtube观看视频</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/551511042204493/">在Facebook观看视频</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动 - 人工智能（AI）</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/10/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/10/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<a id="episode11"></a>
<div style="clear: both;"></div>
<h2>第11集 - 与苏珊·沃西基和琪娜·安妮·麦克兰一起探讨抽象概念</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看第6/3集视频</h4>
<a href="https://www.youtube.com/watch?v=LPnRS3F0VWw"><img src="/images/marketing/episode-11-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=LPnRS3F0VWw"><i>在Youtube观看视频</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/581342096133818/">在Facebook观看视频</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动 - 抽象化</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/11/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/11/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<a id="episode12"></a>
<div style="clear: both;"></div>
<h2>第12集 - 与埃洛·布莱克、布雷特·泰勒和宝拉·梅贾·米那亚一起探讨硬件</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">观看第6/10集视频</h4>
<a href="https://www.youtube.com/watch?v=4-JZYqk2K6w"><img src="/images/marketing/episode-12-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=4-JZYqk2K6w"><i>在Youtube观看视频</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/258344332114141/">在Facebook观看视频</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">每周活动 - 硬件</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">年轻的学生：</span>不用登录</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/12/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（6-12岁）</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">较大的学生：</span>需要登录</p>
<a href="https://studio.code.org/s/code-break/lessons/12/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">出发（13+岁）</button></a>
</div>

</div>

<div style="clear: both"></div>

<hr />

<div style="text-align:center">
<img src="/images/athome/unescovid19.png" style="width:35%">
</div>

