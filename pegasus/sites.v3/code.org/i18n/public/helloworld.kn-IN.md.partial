---
title: ಹಲೋ ವರ್ಲ್ಡ್
theme: responsive
set_dir: true
social:
  og:title: Hello World!
  twitter:title: Hello World!
  og:description: Choose from fun themes to code interactive characters in a world you create in this Hour of Code activity!
  twitter:description: Choose from fun themes to code interactive characters in a world you create in this Hour of Code activity
  og:image: https://code.org/images/social-media/helloworld-og-image-1200x630.png
  og:image:width: 1200
  og:image:height: 630
  twitter:card: summary_large_image
  twitter:image:src: https://code.org/images/social-media/helloworld-og-image-1200x630.png
---

# ಹಲೋ ವರ್ಲ್ಡ್!

ಕಂಪ್ಯೂಟರ್ ಸೇನ್ಸ್ 101 ರಲ್ಲಿ, ಅನೇಕ ವಿದ್ಯಾರ್ಥಿಗಳು ರೂಪಿಸುವ ಮೊದಲ ಪ್ರೋಗ್ರಾಂ ಸರಳವಾಗಿದ್ದು ಈ ವಿಶಿಷ್ಟ ಬರಹದ ಸಾಲನ್ನು ಔಟ್‌ಪುಟ್ ಆಗಿ ನೀಡುತ್ತದೆ: "ಹಲೋ ವರ್ಲ್ಡ್!" ಈ ಪರಿಚಯಾತ್ಮಕ ಚಟುವಟಿಕೆಯೊಂದಿಗೆ ಕಂಪ್ಯೂಟರ್ ವಿಜ್ಞಾನದ ಜಗತ್ತಿಗೆ ಹಲೋ ಹೇಳಿ ಅದು ವಿದ್ಯಾರ್ಥಿಗಳನ್ನು ಮೂಲಭೂತ ಕೋಡಿಂಗ್ ಕೌಶಲ್ಯ ಮತ್ತು ಅಪ್ಲಿಕೇಶನ್‌ಗಳನ್ನು ರಚಿಸಲು ಆತ್ಮವಿಶ್ವಾಸವನ್ನು ನೀಡುತ್ತದೆ. ನೀವು ರಚಿಸುವ ಜಗತ್ತಿನಲ್ಲಿ ಸಂವಾದಾತ್ಮಕ ಅಕ್ಷರಗಳನ್ನು ಕೋಡ್ ಮಾಡಲು ಆರು ಮೋಜಿನ ಥೀಮ್‌ಗಳಿಂದ ಆರಿಸಿಕೊಳ್ಳಿ!

## Choose your Hello World theme

{{ spritelab_module, title: "ಹೊಸತು! Space", color: "#d41f33", image: "images/csc/helloworld/helloworldspacetheme.png", alt_text: "Two cute astronaut and corgi characters floating in space", url: "https://studio.code.org/s/hello-world-space-2022/reset" }}
{{ spritelab_module, title: "ಹೊಸತು! Soccer", color: "#3ba467", image: "images/csc/helloworld/helloworldsoccertheme.png", alt_text: "Three giraffe, tiger, and rhino characters on a field about to start a game of soccer", url: "https://studio.code.org/s/hello-world-soccer-2022/reset" }}
{{ spritelab_module, title: "Food", color: "#C14790", image: "images/csc/helloworld/helloworldhappyfoodtheme.png", alt_text: "Three smiling pizza, cheeseburger, and taco characters on a plate", url: "https://studio.code.org/s/hello-world-food-2021/reset" }}
{{spritelab_module, title: “ಪ್ರಾಣಿಗಳು”, color: “var (--brand_primary_default)”, image: "images/csc/helloworld/helloworldanimalstheme.png “, alt_text: “ಸೂರ್ಯ ಹೊಳೆಯುವ ಹುಲ್ಲಿನ ಮೈದಾನದಲ್ಲಿ ಮೂರು ಹುಲಿ, ಕರಡಿ, ಮತ್ತು ಆನೆ ಪಾತ್ರಗಳು”, url: "https://studio.code.org/s/hello-world-animals-2021/reset"}}
{{ spritelab_module, title: "Retro", color: "#FFA400", image: "images/csc/helloworld/helloworldretrotheme.png", alt_text: "An 8-bit game with alien characters moving through a maze ", url: "https://studio.code.org/s/hello-world-retro-2021/reset" }}
{{ spritelab_module, title: "Emoji", color: "#0094CA", image: "images/csc/helloworld/helloworldemojitheme.png", alt_text: "Three fun emoji characters with different expressions in front of a disco ball", url: "https://studio.code.org/s/hello-world-emoji-2021/reset" }}
{{ clearfix }}

{{ teacher_info, heading: "ಶಿಕ್ಷಕರ ಮಾಹಿತಿ", ಶೀರ್ಷಿಕೆ: "ಹಲೋ ವರ್ಲ್ಡ್ ನಂತರ ಕಲಿಕೆಯನ್ನು ಮುಂದುವರಿಸುವುದು ಹೇಗೆ", ವಿವರಣೆ: "ನಮ್ಮ ಹೊಸ ಪಠ್ಯಕ್ರಮ, ಸಿಎಸ್ ಕನೆಕ್ಷನ್‌‌ಗಳಲ್ಲಿ ಓಪನ್ ಎಂಡೆಡ್ ಪ್ರಾಜೆಕ್ಟುಗಳನ್ನು ಪ್ರಯತ್ನಿಸುವುದಕ್ಕೂ ಮೊದಲು ತರಗತಿಯ ಕೋಣೆಯಲ್ಲಿ ಹಲೋ ವರ್ಲ್ಡ್ ನಿಖರವಾದ ಮೊದಲ ಹೆಜ್ಜೆಯಾಗಿದೆ! ಕೋಡಿಂಗ್ ಮೂಲಾಂಶಗಳನ್ನು ಕಲಿತ ನಂತರ, ಸಿಎಸ್ ಕನೆಕ್ಷನ್ಸ್‌ ಕ್ರಾಸ್ ಸರ್ಕ್ಯುಲರ್ ಪ್ರಾಜೆಕ್ಟ್‌ಗಳಿಗೆ ವಿದ್ಯಾರ್ಥಿಗಳು ಸಾಗಬಹುದು.", button_text: "ಇನ್ನಷ್ಟು ಕಲಿಯಿರಿ", url: "/educate/csc" }}

## ವಿದ್ಯಾರ್ಥಿ ರಚನೆಗಳನ್ನು ಒಳಗೊಂಡಿದೆ

{{ featured_project, title: "Food", author: "M", age: "13+", image: "images/csc/helloworld/cschelloworld_happyfood2.gif", alt_text: "A student project featuring an animated gif of a fork moving across a plate next to avocado and pizza characters", url: "https://studio.code.org/projects/spritelab/sC_ZiNi_x5GUqsWHE2M4CrcbjU8XvtD3VNT7TM0Y0N8" }}
{{ featured_project, title: "Emoji", author: "D", age: "8+", image: "images/csc/helloworld/cschelloworld_emoji.gif", alt_text: "A student project featuring an animated gif of different emojis that say Click Me", url: "https://studio.code.org/projects/spritelab/9HGWXijqhLzaIIUQbPXlNmWgMO1SXzf3TvMHNtbOXmc" }}
{{ featured_project, title: "Animals", author: "B", age: "18+", image: "images/csc/helloworld/cschelloworld_animals.gif", alt_text: "A student project featuring an animated gif of an underwater scene with dolphin, fish, and jellyfish characters in front of a shipwreck", url: "https://studio.code.org/projects/spritelab/rYH8D8eAvWOjuiOpWbHzN4HAtis4ykKTIjGcNPP9zD4" }}
{{ featured_project, title: "Retro", author: "W", age: "13+", image: "images/csc/helloworld/cschelloworld_retro.gif", alt_text: "A student project featuring an animated gif of a game of tag with two alien characters on a distant planet", url: "https://studio.code.org/projects/spritelab/rYH8D8eAvWOjuiOpWbHzN7yo2E1S0q87VqlzaBz7oqo" }}
{{ clearfix }}

{{ helloworld }}
