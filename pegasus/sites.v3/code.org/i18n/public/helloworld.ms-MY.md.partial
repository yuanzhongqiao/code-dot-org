---
title: Helo Dunia
theme: responsive
set_dir: true
social:
  og:title: Hello World!
  twitter:title: Hello World!
  og:description: Choose from fun themes to code interactive characters in a world you create in this Hour of Code activity!
  twitter:description: Choose from fun themes to code interactive characters in a world you create in this Hour of Code activity
  og:image: https://code.org/images/social-media/helloworld-og-image-1200x630.png
  og:image:width: 1200
  og:image:height: 630
  twitter:card: summary_large_image
  twitter:image:src: https://code.org/images/social-media/helloworld-og-image-1200x630.png
---

# Helo Dunia!

Dalam Sains Komputer 101, atur cara pertama yang dicipta oleh ramai pelajar ialah atur cara ringkas yang menghasilkan baris teks ikonik: "Helo Dunia!" Bertanya khabar kepada dunia sains komputer dengan aktiviti pengenalan ini yang melengkapkan pelajar dengan kemahiran pengekodan asas dan keyakinan untuk mencipta apl. Pilih daripada enam tema yang menyeronokkan kepada kod aksara interaktif dalam dunia yang anda cipta!

## Choose your Hello World theme

{{spritelab_module, title: “BARU! Space", color: "#d41f33", image: "images/csc/helloworld/helloworldspacetheme.png", alt_text: "Two cute astronaut and corgi characters floating in space", url: "https://studio.code.org/s/hello-world-space-2022/reset" }}
{{spritelab_module, title: “BARU! Soccer", color: "#3ba467", image: "images/csc/helloworld/helloworldsoccertheme.png", alt_text: "Three giraffe, tiger, and rhino characters on a field about to start a game of soccer", url: "https://studio.code.org/s/hello-world-soccer-2022/reset" }}
{{ spritelab_module, title: "Food", color: "#C14790", image: "images/csc/helloworld/helloworldhappyfoodtheme.png", alt_text: "Three smiling pizza, cheeseburger, and taco characters on a plate", url: "https://studio.code.org/s/hello-world-food-2021/reset" }}
{{spritelab_module, title: “Haiwan”, color: “var (--brand_primary_default)”, image: "images/csc/helloworld/helloworldanimalstheme.png “, alt_text: “Tiga watak harimau, beruang, dan gajah di ladang berumput dengan matahari bersinar”, url: "https://studio.code.org/s/hello-world-animals-2021/reset"}}
{{ spritelab_module, title: "Retro", color: "#FFA400", image: "images/csc/helloworld/helloworldretrotheme.png", alt_text: "An 8-bit game with alien characters moving through a maze ", url: "https://studio.code.org/s/hello-world-retro-2021/reset" }}
{{ spritelab_module, title: "Emoji", color: "#0094CA", image: "images/csc/helloworld/helloworldemojitheme.png", alt_text: "Three fun emoji characters with different expressions in front of a disco ball", url: "https://studio.code.org/s/hello-world-emoji-2021/reset" }}
{{ clearfix }}

{{ teacher_info, heading: "Maklumat Guru", title: "Cara meneruskan pembelajaran selepas Helo Dunia", description: "Helo Dunia ialah langkah pertama yang sempurna untuk kelas sebelum mencuba projek terbuka dalam kurikulum baharu kita, Penyambungan CS! Setelah mempelajari beberapa asas pengekodan, pelajar akan dapat maju ke projek rentas kurikulum Sambungan SK.", button_text: "Ketahui lebih lanjut", url: "/educate/csc" }}

## Ciptaan Pelajar Yang Diketengahkan

{{ featured_project, title: "Food", author: "M", age: "13+", image: "images/csc/helloworld/cschelloworld_happyfood2.gif", alt_text: "A student project featuring an animated gif of a fork moving across a plate next to avocado and pizza characters", url: "https://studio.code.org/projects/spritelab/sC_ZiNi_x5GUqsWHE2M4CrcbjU8XvtD3VNT7TM0Y0N8" }}
{{ featured_project, title: "Emoji", author: "D", age: "8+", image: "images/csc/helloworld/cschelloworld_emoji.gif", alt_text: "A student project featuring an animated gif of different emojis that say Click Me", url: "https://studio.code.org/projects/spritelab/9HGWXijqhLzaIIUQbPXlNmWgMO1SXzf3TvMHNtbOXmc" }}
{{ featured_project, title: "Animals", author: "B", age: "18+", image: "images/csc/helloworld/cschelloworld_animals.gif", alt_text: "A student project featuring an animated gif of an underwater scene with dolphin, fish, and jellyfish characters in front of a shipwreck", url: "https://studio.code.org/projects/spritelab/rYH8D8eAvWOjuiOpWbHzN4HAtis4ykKTIjGcNPP9zD4" }}
{{ featured_project, title: "Retro", author: "W", age: "13+", image: "images/csc/helloworld/cschelloworld_retro.gif", alt_text: "A student project featuring an animated gif of a game of tag with two alien characters on a distant planet", url: "https://studio.code.org/projects/spritelab/rYH8D8eAvWOjuiOpWbHzN7yo2E1S0q87VqlzaBz7oqo" }}
{{ clearfix }}

{{ helloworld }}
