---
title: Xin chào cả thế giới
theme: responsive
set_dir: true
social:
  og:title: Hello World!
  twitter:title: Hello World!
  og:description: Choose from fun themes to code interactive characters in a world you create in this Hour of Code activity!
  twitter:description: Choose from fun themes to code interactive characters in a world you create in this Hour of Code activity
  og:image: https://code.org/images/social-media/helloworld-og-image-1200x630.png
  og:image:width: 1200
  og:image:height: 630
  twitter:card: summary_large_image
  twitter:image:src: https://code.org/images/social-media/helloworld-og-image-1200x630.png
---

# Xin chào cả thế giới!

Trong khóa học Khoa học Máy tính 101, chương trình đầu tiên mà nhiều học viên tạo ra là một chương trình đơn giản cho đầu ra là một dòng văn bản kinh điển: "Hello World!" (Xin chào thế giới!) Hãy nói lời chào với cả thế giới khoa học máy tính trong hoạt động giới thiệu nhằm trang bị cho học sinh những kỹ năng lập trình căn bản và sự tự tin để sáng tạo ứng dụng. Hãy chọn trong sáu chủ đề thú vị để lập trình các nhân vật tương tác trong thế giới do chính bạn tạo ra!

## Choose your Hello World theme

{{ spritelab_module, title: "MỚI! Space", color: "#d41f33", image: "images/csc/helloworld/helloworldspacetheme.png", alt_text: "Two cute astronaut and corgi characters floating in space", url: "https://studio.code.org/s/hello-world-space-2022/reset" }}
{{ spritelab_module, title: "MỚI! Soccer", color: "#3ba467", image: "images/csc/helloworld/helloworldsoccertheme.png", alt_text: "Three giraffe, tiger, and rhino characters on a field about to start a game of soccer", url: "https://studio.code.org/s/hello-world-soccer-2022/reset" }}
{{ spritelab_module, title: "Food", color: "#C14790", image: "images/csc/helloworld/helloworldhappyfoodtheme.png", alt_text: "Three smiling pizza, cheeseburger, and taco characters on a plate", url: "https://studio.code.org/s/hello-world-food-2021/reset" }}
{{spritelab_module, title: “Động vật”, color: “var (--brand_primary_default)”, image: "images/csc/helloworld/helloworldanimalstheme.png “, alt_text: “Ba ký tự hổ, gấu và voi trên cánh đồng cỏ với ánh mặt trời chiếu sáng”, url: "https://studio.code.org/s/hello-world-animals-2021/reset"}}
{{ spritelab_module, title: "Retro", color: "#FFA400", image: "images/csc/helloworld/helloworldretrotheme.png", alt_text: "An 8-bit game with alien characters moving through a maze ", url: "https://studio.code.org/s/hello-world-retro-2021/reset" }}
{{ spritelab_module, title: "Emoji", color: "#0094CA", image: "images/csc/helloworld/helloworldemojitheme.png", alt_text: "Three fun emoji characters with different expressions in front of a disco ball", url: "https://studio.code.org/s/hello-world-emoji-2021/reset" }}
{{ clearfix }}

{{ teacher_info, heading: "Thông tin giáo viên", title: "Làm thế nào để tiếp tục học sau Hello World", description: "Hello World là bước đầu tiên hoàn hảo cho các lớp học trước khi thử các dự án mở trong chương trình học mới của chúng tôi, CS Connections! Sau khi học kiến thức căn bản về lập trình, học sinh sẽ có thể chuyển sang các dự án kết hợp nhiều môn học của Kết nối KHMT.", button_text: "Tìm hiểu thêm", url: "/educate/csc" }}

## Tác phẩm nổi bật của học sinh

{{ featured_project, title: "Food", author: "M", age: "13+", image: "images/csc/helloworld/cschelloworld_happyfood2.gif", alt_text: "A student project featuring an animated gif of a fork moving across a plate next to avocado and pizza characters", url: "https://studio.code.org/projects/spritelab/sC_ZiNi_x5GUqsWHE2M4CrcbjU8XvtD3VNT7TM0Y0N8" }}
{{ featured_project, title: "Emoji", author: "D", age: "8+", image: "images/csc/helloworld/cschelloworld_emoji.gif", alt_text: "A student project featuring an animated gif of different emojis that say Click Me", url: "https://studio.code.org/projects/spritelab/9HGWXijqhLzaIIUQbPXlNmWgMO1SXzf3TvMHNtbOXmc" }}
{{ featured_project, title: "Animals", author: "B", age: "18+", image: "images/csc/helloworld/cschelloworld_animals.gif", alt_text: "A student project featuring an animated gif of an underwater scene with dolphin, fish, and jellyfish characters in front of a shipwreck", url: "https://studio.code.org/projects/spritelab/rYH8D8eAvWOjuiOpWbHzN4HAtis4ykKTIjGcNPP9zD4" }}
{{ featured_project, title: "Retro", author: "W", age: "13+", image: "images/csc/helloworld/cschelloworld_retro.gif", alt_text: "A student project featuring an animated gif of a game of tag with two alien characters on a distant planet", url: "https://studio.code.org/projects/spritelab/rYH8D8eAvWOjuiOpWbHzN7yo2E1S0q87VqlzaBz7oqo" }}
{{ clearfix }}

{{ helloworld }}
