---
title: Neem een Code Break
video_player: true
theme: responsive
social:
  og:title: Code.org Code Break
  og:description: Learn CS with Hadi Partovi and the team at Code.org with a live weekly webcast and weekly challenges for students of all abilities
  og:image: http://code.org/images/marketing/announcement_special2020.jpg
  og:image:width: 850
  og:image:height: 502
  og:video: ''
  og:video:width: ''
  og:video:height: ''
  og:video:type: ''
---

<div style="background-color: var(--brand_primary_default); padding: 15px; margin-top:50px;"><img src="/images/athome/CodeBreak-horizontal.png" style="max-width: 100%"/></div>
<h1>Neem een Code Break</h1>
<div class="col-66" style="padding-right: 28px; padding-bottom: 20px;">

<p>Hou Computerwetenschap leuk door een Code Break te nemen! In een reeks <a href="#activities"> episoden en activitieiten</a> je kunt kiezen tussen leren coderen van Ashton Kutcher, meer over variabelen komen te weten van Yara Shahidi, of ontdekken hoe het internet werkt van de eigenlijke uitvinder van internet zelf, dhr. Vint Cerf.</p>

<p>Elke aflevering wordt geleid door de oprichter van Code.org, <a href="https://code.org/about/leadership/hadi_partovi">Hadi Partovi</a>, inclusief inspirerende gasten, minilessen over computerwetenschappelijke concepten en een wekelijkse challenge om studenten van alle leeftijden en vaardigheden te betrekken, zelfs degenen zonder computers.</p>

<a href="#activities"><button style="background-color: #ffa400;border-color: #ffa400;color: #FFFFFF;font-size: 16px;">Toon afleveringen</button></a>

<p><br>Hoewel onze livestreams voorbij zijn, je kunt altijd nog eerdere afleveringen bekijken en de activiteiten voltooien. Voor nog meer leerplezier, kun je ook enkele van onze aanbevolen projecten en activiteiten voor <a href="https://code.org/athome">thuis leren</a> uitproberen.</p>
<br>

<div style="max-width:490px; position:relative;">{{ codebreak_promoreel }}</div>
<p>Volg ons op <a href="http://facebook.com/code.org">Facebook</a> of op <a href="http://twitter.com/codeorg">Twitter</a>.</p>
</div>

<div class="col-33" style=" border: solid 1px #bbb; border-radius: 5px; overflow: hidden; position: relative; float: left; box-sizing: border-box;">
<img src="/images/marketing/code-break-callout-episode-0.jpg" style="max-width:100%">
<a id="signup"></a>
<div style="padding:8px 18px 0 8px;">
<p style="margin: 10px 0 0 0;">Blijf jezelf ontwikkelen! Schrijf je in op onze mailinglijst om Code.org updates te ontvangen en wees als eerste op de hoogte te zijn van de nieuwe bronnen voor thuisonderwijs.</p>
<iframe src="http://go.pardot.com/l/153401/2020-03-16/n7j6zc" width="100%" height="305" type="text/html" frameborder="0" allowTransparency="true" style="overflow:hidden"></iframe>

</div></div>

<div style="clear: both;"></div>
<div style="max-width:100%;">
<h2>Leuk en fundamenteel</h2>
<p>Computerwetenschap is <i>super leuk</i> omdat het creatief en speelvol is. Of het nu gaat om het oplossen van een puzzel, een kunstproject coderen, of een app ontwerpen, studenten leren liever door te ontdekken dan door te onthouden. Studenten zelfs aangeven CS als hun <a href="https://code.org/promote">favoriete vak</a> voor de schoolvakken als kunst, muziek of dans.</p>
<p>Computerwetenschap is <a href="/promote">fundamenteel</a> voor alle studierichtingen, maar aangezien dat er nog weinig scholen het aanbieden, dit kan een unieke kans zijn om je kind te ondersteunen bij een nieuwe en leuke leermogelijkheid. Sterker nog, <a href="https://medium.com/@codeorg/cs-helps-students-outperform-in-school-college-and-workplace-66dd64a69536">recente studies aantonen dat</a>: kinderen die informatica studeren, presteren beter in andere vakken, blinken uit in het oplossen van problemen en ze maken <font color="00adbc"><b>17% meer kans op een universiteit te gaan studeren</font></b>.</p></div>

<div style="clear: both;"></div>
<a id="activities"></a>

<div style="background-color: var(--brand_secondary_default); margin-top:50px;"><h1 style="color: #FFFFFF; padding-top: 15px; padding-bottom: 15px; padding-left: 10px; padding-right: 10px;">Wekelijkse activiteiten &amp; Uitzendingen gemist</h1></div>
<h2>Aflevering 1 - Algoritmen met Hill Harper</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 3/25</h4>
<a href="https://www.youtube.com/watch?v=IiHveVqxvcw"><img src="/images/marketing/episode-1-hill-harper.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br> <a href="https://www.youtube.com/watch?v=IiHveVqxvcw"><i>Bekijk het op Youtube</a></i> I <i><a href="https://www.facebook.com/Code.org/videos/212168439992679/">Bekijk het op Facebook</i></a></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteiten - Algoritmen</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="http://studio.code.org/s/code-break-younger/lessons/1/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="http://studio.code.org/s/code-break/lessons/1/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<div style="clear: both;"></div>
<h2>Aflevering 2 - Prototypes met Mark Cuban & Lyndsey Scott</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 4/1</h4>
<a href="https://www.youtube.com/watch?v=guNM-3CXgnQ"><img src="/images/marketing/code-break-episode2-screenshot.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=guNM-3CXgnQ"><i>Bekijk het op Youtube</i></a> I <a href="https://www.facebook.com/Code.org/videos/231140824935457/"><i>Bekijk het op Facebook</i></a></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteiten - Prototypes</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/2/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/2/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<a id="episode3"></a>
<div style="clear: both;"></div>
<h2>Aflevering 3 - Encryptie met Ashton Kutcher & Mia Gil Epner</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 4/8</h4>
<a href="https://youtu.be/_RrmereHcTQ"><img src="/images/marketing/code-break-april-8-video-final.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://youtu.be/_RrmereHcTQ"><i>Bekiijk het op Youtube</i></a> I <a href="https://www.facebook.com/Code.org/videos/221983958869058/"><i>Bekiijk het op Facebook</i></a></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteit - Encryptie</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/3/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/3/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<div style="clear: both;"></div>

<a id="episode4"></a>
<div style="clear: both;"></div>
<h2>Aflevering 4 - Digitale informatie met Mike Krieger & Alice Keeler</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 4/15</h4>
<a href="https://www.youtube.com/watch?v=kL-3o5EiLsM"><img src="/images/marketing/code-break-april-15-video.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=kL-3o5EiLsM"><i>Bekijk het op Youtube</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/1642489922621088/
">Bekijk het op Facebook</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteit - Digitale informatie</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/4/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/4/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<div style="clear: both;"></div>

<a id="episode5"></a>
<div style="clear: both;"></div>
<h2>Aflevering 5 - Simulatie & Data met Bill Gates</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 4/22</h4>
<a href="https://www.youtube.com/watch?v=fv5g9xw_jek"><img src="/images/marketing/code-break-bill-gates.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=fv5g9xw_jek"><i>Bekijk het op Youtube</i></a> I <i><a href="https://www.facebook.com/watch/live/?v=870396600091039&ref=watch_permalink">Bekijk het op Facebook</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteit - Simulatie & Data</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/5/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/5/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<div style="clear: both;"></div>

<a id="episode6"></a>
<div style="clear: both;"></div>
<h2>Aflevering 6 - Het internet met Keegan-Michael Key & Vint Cerf</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 4/29</h4>
<a href="https://www.youtube.com/watch?v=7jcnJR6aZfs"><img src="/images/marketing/code-break-episode-6.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=7jcnJR6aZfs"><i>Bekijk het op Youtube</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/267326194655266/">Bekijk het op Facebook</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteit - Het Internet</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/6/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/6/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<div style="clear: both;"></div>

<a id="episode7"></a>
<div style="clear: both;"></div>
<h2>Aflevering 7 - Conditionals met Sal Khan & Flo Vaughn</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 5/6</h4>
<a href="https://www.youtube.com/watch?v=oKwvRwmvXsE"><img src="/images/marketing/episode-7-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=oKwvRwmvXsE"><i>Bekijk het op Youtube</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/838885113258946/">Bekijk het op Facebook</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteit - Conditionals</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/7/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/7/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<a id="episode8"></a>
<div style="clear: both;"></div>
<h2>Aflevering 8 - Variabelen met Yara Shahidi & Fuzzy Khosrowshahi</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 5/13</h4>
<a href="https://www.youtube.com/watch?v=bQElba6x6yI"><img src="/images/marketing/episode-8-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=bQElba6x6yI"><i>Bekijk het op Youtube</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/580781632554625/"> Bekijk het op Facebook</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteit - Variabelen</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/8/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/8/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<a id="episode9"></a>
<div style="clear: both;"></div>
<h2>Aflevering 9 - Evenementen met Macklemore & Scott Forstall</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 5/20</h4>
<a href="https://www.youtube.com/watch?v=-bcO-X9thds"><img src="/images/marketing/episode-9-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=-bcO-X9thds"><i>Bekijk het op Youtube</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/3261824343882823/">Bekijk het op Facebook</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteit - Evenementen</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/9/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/9/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<a id="episode10"></a>
<div style="clear: both;"></div>
<h2>Aflevering 10 - Kunstmatige intelligentie met Kat Graham & Kate Park</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 5/27</h4>
<a href="https://www.youtube.com/watch?v=MLH2bKpXkm8"><img src="/images/marketing/episode-10-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=MLH2bKpXkm8"><i>Bekijk het op Youtube</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/551511042204493/">Bekijk het op Facebook</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteit - Kunstmatige intelligentie (AI)</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/10/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/10/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<a id="episode11"></a>
<div style="clear: both;"></div>
<h2>Aflevering 11- Abstractie met Susan Wojcicki & China Anne McClain</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 6/3</h4>
<a href="https://www.youtube.com/watch?v=LPnRS3F0VWw"><img src="/images/marketing/episode-11-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=LPnRS3F0VWw"><i>Bekijk het op Youtube</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/581342096133818/">Bekijk het op Facebook</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteit - Abstractie</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/11/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/11/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<a id="episode12"></a>
<div style="clear: both;"></div>
<h2>Aflevering 12 - Hardware met Aloe Blacc, Bret Taylor, & Paola Mejia Minaya</h2>
<div class="col-33" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Bekijk de aflevering 6/10</h4>
<a href="https://www.youtube.com/watch?v=4-JZYqk2K6w"><img src="/images/marketing/episode-12-screengrab.png" class="responsive" style="padding-bottom: 10px; max-width: 100%"></a><br><a href="https://www.youtube.com/watch?v=4-JZYqk2K6w"><i> Bekijk het op YouTube</i></a> I <i><a href="https://www.facebook.com/Code.org/videos/258344332114141/">Bekijk het op Facebook</a></i></div>

<div class="col-66" style="padding-right:20px;">
<h4 style="background:var(--brand_primary_default);color:#ffffff; padding:8px;">Wekelijkse activiteit - Hardware</h4>

<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold"> Voor jonge studenten:</span> aanmelden is niet nodig</p>
<a href="https://studio.code.org/s/code-break-younger/lessons/12/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 6-12)</button></a>
</div>
<div class="col-50" style="padding-right:20px; padding-bottom: 30px;">
<p><span style="font-weight:bold">Voor studenten middelbare school:</span> aanmelden is vereist</p>
<a href="https://studio.code.org/s/code-break/lessons/12/levels/1"><button style="background-color: #e7e8ea;border-color: #e7e8ea;color: #5b6770;">Ga (leeftijdsgroep 13+)</button></a>
</div>

</div>

<div style="clear: both"></div>

<hr />

<div style="text-align:center">
<img src="/images/athome/unescovid19.png" style="width:35%">
</div>

