---
title: ஹலோ வேர்ல்டு
theme: responsive
set_dir: true
social:
  og:title: Hello World!
  twitter:title: Hello World!
  og:description: Choose from fun themes to code interactive characters in a world you create in this Hour of Code activity!
  twitter:description: Choose from fun themes to code interactive characters in a world you create in this Hour of Code activity
  og:image: https://code.org/images/social-media/helloworld-og-image-1200x630.png
  og:image:width: 1200
  og:image:height: 630
  twitter:card: summary_large_image
  twitter:image:src: https://code.org/images/social-media/helloworld-og-image-1200x630.png
---

# வணக்கம், உலகம்!

கணினியியல் 101 -இல், பல மாணவர்கள் உருவாக்கும் முதல் நிரலானது ஒரு சின்னமான உரையை வெளியிடும் வகையில் எளிமையானது: "ஹலோ வோர்ல்ட்!" இந்த அறிமுகச் செயல்பாட்டின் மூலம் கணினி அறிவியல் உலகிற்கு வணக்கம் சொல்லுங்கள், இது மாணவர்களுக்கு அடிப்படை குறியீட்டு திறன்கள் மற்றும் பயன்பாடுகளை உருவாக்குவதற்கான நம்பிக்கையை அளிக்கிறது. நீங்கள் உருவாக்கும் உலகில் உள்ள ஊடாடும் எழுத்துக்களுக்கு ஆறு வேடிக்கையான தீம்களில் இருந்து தேர்வு செய்யவும்!

## Choose your Hello World theme

{{ spritelab_module, title: "பதிய! Space", color: "#d41f33", image: "images/csc/helloworld/helloworldspacetheme.png", alt_text: "Two cute astronaut and corgi characters floating in space", url: "https://studio.code.org/s/hello-world-space-2022/reset" }}
{{ spritelab_module, title: "பதிய! Soccer", color: "#3ba467", image: "images/csc/helloworld/helloworldsoccertheme.png", alt_text: "Three giraffe, tiger, and rhino characters on a field about to start a game of soccer", url: "https://studio.code.org/s/hello-world-soccer-2022/reset" }}
{{ spritelab_module, title: "Food", color: "#C14790", image: "images/csc/helloworld/helloworldhappyfoodtheme.png", alt_text: "Three smiling pizza, cheeseburger, and taco characters on a plate", url: "https://studio.code.org/s/hello-world-food-2021/reset" }}
{{spritelab_module, title: “விலங்குகள்”, color: “var (--brand_primary_default)”, image: "images/csc/helloworld/helloworldanimalstheme.png “, alt_text: “சூரியன் பிரகாசிக்கும் புல் வயலில் மூன்று புலி, கரடி மற்றும் யானை எழுத்துக்கள்”, url: "https://studio.code.org/s/hello-world-animals-2021/reset"}}
{{ spritelab_module, title: "Retro", color: "#FFA400", image: "images/csc/helloworld/helloworldretrotheme.png", alt_text: "An 8-bit game with alien characters moving through a maze ", url: "https://studio.code.org/s/hello-world-retro-2021/reset" }}
{{ spritelab_module, title: "Emoji", color: "#0094CA", image: "images/csc/helloworld/helloworldemojitheme.png", alt_text: "Three fun emoji characters with different expressions in front of a disco ball", url: "https://studio.code.org/s/hello-world-emoji-2021/reset" }}
{{ clearfix }}

{{ teacher_info, heading: "ஆசியர் தகவல்", title: "ஹலோ வோர்ல்டு-க்குப் பிறகு எவ்வாறு தொடர்ந்து கற்றுக்கொள்வது", description: "ஹலோ வேர்ல்ட் என்பது எங்கள் புதிய பாடத்திட்டமான சிஎஸ் இணைப்புகளுக்குள் திறந்தநிலை திட்டப்பணிகளை முயற்சிக்கும் முன் வகுப்பறைகளுக்கான சரியான முதல் படியாகும்! சில குறியீட்டு அடிப்படைகளைக் கற்றுக்கொண்ட பிறகு, மாணவர்கள் CS இணைப்புகளின் குறுக்கு-பாடத்திட்டத் திட்டங்களுக்கு முன்னேற முடியும்.", button_text: "மேலும் அறிக", url: "/educate/csc" }}

## சிறப்பு மாணவர் படைப்புகள்

{{ featured_project, title: "Food", author: "M", age: "13+", image: "images/csc/helloworld/cschelloworld_happyfood2.gif", alt_text: "A student project featuring an animated gif of a fork moving across a plate next to avocado and pizza characters", url: "https://studio.code.org/projects/spritelab/sC_ZiNi_x5GUqsWHE2M4CrcbjU8XvtD3VNT7TM0Y0N8" }}
{{ featured_project, title: "Emoji", author: "D", age: "8+", image: "images/csc/helloworld/cschelloworld_emoji.gif", alt_text: "A student project featuring an animated gif of different emojis that say Click Me", url: "https://studio.code.org/projects/spritelab/9HGWXijqhLzaIIUQbPXlNmWgMO1SXzf3TvMHNtbOXmc" }}
{{ featured_project, title: "Animals", author: "B", age: "18+", image: "images/csc/helloworld/cschelloworld_animals.gif", alt_text: "A student project featuring an animated gif of an underwater scene with dolphin, fish, and jellyfish characters in front of a shipwreck", url: "https://studio.code.org/projects/spritelab/rYH8D8eAvWOjuiOpWbHzN4HAtis4ykKTIjGcNPP9zD4" }}
{{ featured_project, title: "Retro", author: "W", age: "13+", image: "images/csc/helloworld/cschelloworld_retro.gif", alt_text: "A student project featuring an animated gif of a game of tag with two alien characters on a distant planet", url: "https://studio.code.org/projects/spritelab/rYH8D8eAvWOjuiOpWbHzN7yo2E1S0q87VqlzaBz7oqo" }}
{{ clearfix }}

{{ helloworld }}
