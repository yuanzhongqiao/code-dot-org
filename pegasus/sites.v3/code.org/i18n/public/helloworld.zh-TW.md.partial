---
title: 你好世界
theme: responsive
set_dir: true
social:
  og:title: Hello World!
  twitter:title: Hello World!
  og:description: Choose from fun themes to code interactive characters in a world you create in this Hour of Code activity!
  twitter:description: Choose from fun themes to code interactive characters in a world you create in this Hour of Code activity
  og:image: https://code.org/images/social-media/helloworld-og-image-1200x630.png
  og:image:width: 1200
  og:image:height: 630
  twitter:card: summary_large_image
  twitter:image:src: https://code.org/images/social-media/helloworld-og-image-1200x630.png
---

# 你好世界！

在《電腦科學入門》中，多數學生創建的第一個程式便是一個簡單的程式，能夠輸出一行標誌性的文本：「Hello World!」 通過這個介紹活動，向電腦科學的世界問好，讓學生們具備基本的程式設計技能和創建應用程式的信心。 從六個有趣的主題中選擇，在你創建的世界中為交互式角色編碼！

## Choose your Hello World theme

{{ spritelab_module, title: "新的！ Space", color: "#d41f33", image: "images/csc/helloworld/helloworldspacetheme.png", alt_text: "Two cute astronaut and corgi characters floating in space", url: "https://studio.code.org/s/hello-world-space-2022/reset" }}
{{ spritelab_module, title: "新的！ Soccer", color: "#3ba467", image: "images/csc/helloworld/helloworldsoccertheme.png", alt_text: "Three giraffe, tiger, and rhino characters on a field about to start a game of soccer", url: "https://studio.code.org/s/hello-world-soccer-2022/reset" }}
{{ spritelab_module, title: "Food", color: "#C14790", image: "images/csc/helloworld/helloworldhappyfoodtheme.png", alt_text: "Three smiling pizza, cheeseburger, and taco characters on a plate", url: "https://studio.code.org/s/hello-world-food-2021/reset" }}
{{spritelab_module，標題：「動物」，顏色：「var (--brand_primary_default)」，圖片："images/csc/helloworld/helloworldanimalstheme.png「，alt_text：「陽光照射的草地上的三個老虎、熊和大象角色」，網址："https://studio.code.org/s/hello-world-animals-2021/reset"}}
{{ spritelab_module, title: "Retro", color: "#FFA400", image: "images/csc/helloworld/helloworldretrotheme.png", alt_text: "An 8-bit game with alien characters moving through a maze ", url: "https://studio.code.org/s/hello-world-retro-2021/reset" }}
{{ spritelab_module, title: "Emoji", color: "#0094CA", image: "images/csc/helloworld/helloworldemojitheme.png", alt_text: "Three fun emoji characters with different expressions in front of a disco ball", url: "https://studio.code.org/s/hello-world-emoji-2021/reset" }}
{{ clearfix }}

{{ teacher_info, heading: "教師資訊", title: "在Hello World之後如何繼續學習", description: "Hello World是課堂內完美的第一步，然後可以嘗試我們新教材CS Connections內的各種開放性項目! 在學習了一些程式設計基礎知識後，學生將能夠進展到電腦科學聯繫的跨學科項目。", button_text: "Learn more", url: "/educate/csc" }}

## 精選學生作品

{{ featured_project, title: "Food", author: "M", age: "13+", image: "images/csc/helloworld/cschelloworld_happyfood2.gif", alt_text: "A student project featuring an animated gif of a fork moving across a plate next to avocado and pizza characters", url: "https://studio.code.org/projects/spritelab/sC_ZiNi_x5GUqsWHE2M4CrcbjU8XvtD3VNT7TM0Y0N8" }}
{{ featured_project, title: "Emoji", author: "D", age: "8+", image: "images/csc/helloworld/cschelloworld_emoji.gif", alt_text: "A student project featuring an animated gif of different emojis that say Click Me", url: "https://studio.code.org/projects/spritelab/9HGWXijqhLzaIIUQbPXlNmWgMO1SXzf3TvMHNtbOXmc" }}
{{ featured_project, title: "Animals", author: "B", age: "18+", image: "images/csc/helloworld/cschelloworld_animals.gif", alt_text: "A student project featuring an animated gif of an underwater scene with dolphin, fish, and jellyfish characters in front of a shipwreck", url: "https://studio.code.org/projects/spritelab/rYH8D8eAvWOjuiOpWbHzN4HAtis4ykKTIjGcNPP9zD4" }}
{{ featured_project, title: "Retro", author: "W", age: "13+", image: "images/csc/helloworld/cschelloworld_retro.gif", alt_text: "A student project featuring an animated gif of a game of tag with two alien characters on a distant planet", url: "https://studio.code.org/projects/spritelab/rYH8D8eAvWOjuiOpWbHzN7yo2E1S0q87VqlzaBz7oqo" }}
{{ clearfix }}

{{ helloworld }}
