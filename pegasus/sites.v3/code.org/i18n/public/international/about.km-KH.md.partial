---
title: អំពី
theme: responsive
video_player: true
---

# អំពីយើង

<div class="col-60">
  About Us Code.org® គឺជាស្ថាប័នមិនរកប្រាក់ចំណេញ ដែលធ្វើការផ្តោតលើការពង្រីកវិសាលភាពនៃការប្រើប្រាស់វិទ្យាសាស្ត្រកុំព្យូទ័រនៅក្នុងសាលារៀននិងបង្កើនការចូលរួមពីសំណាក់ស្ត្រីវ័យក្មេងនិងសិស្សមកពីក្រុមភាគតិចផ្សេងទៀត។ ចក្ខុវិស័យរបស់យើងគឺថាសិស្សគ្រប់រូបនៅក្នុងសាលាទាំងអស់មានឱកាសរៀនវិទ្យាសាស្ត្រកុំព្យូទ័រដែលដូចជាជីវវិទ្យាគីមីវិទ្យាឬពិជគណិតអញ្ចឹងដែរ។ យើងផ្តល់ជូននូវកម្មវិធីសិក្សាដែលត្រូវបានប្រើប្រាស់យ៉ាងទូលំទូលាយបំផុតសម្រាប់ការបង្រៀនវិទ្យាសាស្ត្រកុំព្យូទ័រនៅបឋមសិក្សានិងមធ្យមសិក្សាហើយក៏បានរៀបចំជាប្រចាំឆ្នាំនូវផងដែរ<a href="https://hourofcode.com">ម៉ោងនៃកូដ</a> យុទ្ធនាការដែលបានចូលរួមដោយសិស្សចំនួន ១០ ភាគរយនៅលើពិភពលោក។ Code.org ត្រូវបានគាំទ្រដោយម្ចាស់ជំនួយរួមមាន Amazon, Facebook, Google, Infosys Foundation, Microsoft និង<a href="/about/donors">ជាច្រើនទៀត</a>.
</div>

[col-40]

<div style="background-color:#7665a0;height:190px;padding:25px;display:flex;justify-content:center;flex-direction:column">

  <font size="4" color="#FFFFFF"><b>វគ្គសិក្សា Code.org ត្រូវបានប្រើប្រាស់ដោយសិស្សរាប់សិបលាននាក់និងដោយគ្រូមួយលាននាក់នៅលើពិភពលោក។</b></font>

</div>

[/col-40]

<div style="clear:both"></div>

## ប្រវត្តិ

នៅឆ្នាំ ២០១៣ Code.org ត្រូវបានបង្កើតឡើងដោយបងប្អូនភ្លោះ Hadi និង Ali Partovi ជាមួយ [វីដេអូផ្សព្វផ្សាយពីវិទ្យាសាស្ត្រកុំព្យូទ័រ](https://www.youtube.com/watch?v=nKIu9yen5nc). វីដេអូនេះឈរលំដាប់លេខ ១ នៅលើយូធូបរយៈពេលមួយថ្ងៃ ហើយមានសាលារៀនចំនួន ១៥, ០០០ បានទាក់ទងមកយើងសុំជំនួយ។ ចាប់តាំងពីពេលនោះមកយើងបានពង្រីកពីបុគ្គលិកស្ម័គ្រចិត្តដើម្បីបង្កើតអង្គការពេញលក្ខណ: មួយដែលគាំទ្រដល់សកម្មភាពទូទាំងពិភពលោក។ យើងជឿជាក់ថាការអប់រំវិទ្យាសាស្ត្រកុំព្យូទ័រដែលមានគុណភាពគួរតែមានសម្រាប់កុមារគ្រប់រូបមិនមែនសំរាប់តែអ្នកមានសំណាងពីរបីអ្នកនោះទេ។
<br> <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/nKIu9yen5nc" frameborder="0" allowfullscreen></iframe>

## អ្វីដែលយើងធ្វើ

យើងធ្វើការនៅគ្រប់វិសាលភាពនៃការអប់រំ៖ ការរចនា [វគ្គសិក្សាផ្ទាល់ខ្លួនរបស់យើង](https://studio.code.org/courses) ឬធ្វើជាដៃគូជាមួយអ្នកដទៃការបណ្តុះបណ្តាលគ្រូ ការចាប់ដៃគូជាមួយសាលារៀនធំ ៗ ជួយផ្លាស់ប្តូរគោលនយោបាយរដ្ឋាភិបាល ដោយពង្រីកលើឆាកអន្តរជាតិតាមរយៈភាពជាដៃគូនិងផ្សព្វទីផ្សារ ដើម្បីបំបែកគោលគំនិតចាស់។ ការងាររបស់យើងកសាងដោយការខិតខំជាច្រើនទសវត្ស ដោយអង្គការនិងបុគ្គលជាច្រើនរាប់មិនអស់ដែលបានជួយបង្កើតមូលនិធិនិងផ្សព្វផ្សាយការអប់រំវិទ្យាសាស្ត្រកុំព្យូទ័រ។ យើងសូមថ្លែងអំណរគុណចំពោះ សហគមន៍អប់រំវិទ្យាសាស្ត្រកុំព្យូទ័រដែលធ្វើការដោយឥតនឿយហត់ យើងសូមថ្លែងអំណរគុណចំពោះដៃគូនិងបុគ្គលទាំងអស់ដែលបានពង្រីកផ្លែផ្កាលទ្ធផលបស់យើងជាច្រើនឆ្នាំកន្លងមក។

## ការពង្រីកទៅអន្តរជាតិរបស់ Code.org

ជាង ៤០% នៃចរាចរគេហទំព័ររបស់យើងគឺមកពីខាងក្រៅសហរដ្ឋអាមេរិកហើយចំនួននោះនៅតែបន្តកើនឡើង។ ដើម្បីពង្រីកការចូលប្រើវិទ្យាសាស្ត្រកុំព្យូទ័រជាសកល ក្រុមការងាររបស់យើងធ្វើការយ៉ាងជិតស្និទ្ធជាមួយ [ ដៃគូអន្តរជាតិជាង ១០០ ](/about/partners) ដោយជួយពួកគេក្នុងការលើកកម្ពស់មួយម៉ោងនៃកូដ តស៊ូមតិសម្រាប់ការផ្លាស់ប្តូរគោលនយោបាយ។ និងបណ្តុះបណ្តាលគ្រូ។ យើងកំពុងបង្កើតផ្នែកវិទ្យាសាស្ត្រកុំព្យូទ័រជាផ្នែកមួយនៃការអប់រំអន្តរជាតិដោយចាប់ដៃគូជាមួយក្រសួងអប់រំពីជុំវិញពិភពលោកហើយធ្វើការជាមួយអង្គការអន្តរជាតិដូចជាអង្គការសហប្រតិបត្តិការនិងអភិវឌ្ឍន៍សេដ្ឋកិច្ចនិងអង្គការអប់រំវិទ្យាសាស្ត្រនិងវប្បធម៌របស់អង្គការសហប្រជាជាតិ។

<img alt="ដៃគូអន្តរជាតិ Code.org" src="/images/international/international_partners.jpg" width="100%" />

## ការបកប្រែ

ចង់មានការផ្លាស់ប្តូរដោយផ្ទាល់ចំពោះសិស្សនៅក្នុងប្រទេសរបស់អ្នកទេ? ជួយយើងធ្វើឱ្យមាតិកា Code.org អាចប្រើបានជាភាសាកំណើតរបស់ពួកគេ! ប្រសិនបើអ្នកស្ម័គ្រចិត្តជួយបកប្រែការបង្ហាត់បង្រៀននិងមេរៀនរបស់យើងអ្នកនឹងចូលរួមជាមួយសហគមន៍អ្នកបកប្រែជាង ៧, ០០០ នាក់ដែលជួយនាំមកនូវការអប់រំវិទ្យាសាស្ត្រកុំព្យូទ័រដល់សិស្សនៅជុំវិញពិភពលោក។ សូមមើលនៅ [ ការណែនាំការបកប្រែ ](/translate) របស់យើងសម្រាប់ព័ត៌មានបន្ថែម។

## ការប្តេជ្ញាចិត្តរបស់យើងចំពោះកម្មវិធីសិក្សាឥតគិតថ្លៃនិងបច្ចេកវិទ្យាបើកចំហ

រាល់ធនធានកម្មវិធីសិក្សានិងការបង្រៀនដែលយើងបង្កើត នឹងមិនគិតថ្លៃជារៀងរហូត និងបើកចំហរការអនុញ្ញាតជាផ្លូវការក្រោម [ Creative Commons ](http://creativecommons.org/licenses/by-nc-sa/4.0/), ដែលអនុញ្ញាតឱ្យអ្នកផ្សេងទៀតប្រើប្រាស់ឱ្យធនធានអប់រំសម្រាប់គោលបំណងមិនមែនពាណិជ្ជកម្ម។ ប្រសិនបើអ្នកចាប់អារម្មណ៍ក្នុងការផ្តល់អាជ្ញាប័ណ្ណសម្ភារៈរបស់យើងសម្រាប់គោលបំណងពាណិជ្ជកម្ម [ ទាក់ទងយើង ](/contact) ។ វគ្គសិក្សារបស់យើងត្រូវបានបកប្រែសម្រាប់ការប្រើប្រាស់ទូទាំងពិភពលោកឬដោយអ្នកនិយាយភាសាផ្សេងគ្នា។ បច្ចេកវិទ្យារបស់យើងត្រូវបានអភិវឌ្ឍជា [ គម្រោង Open Source ](https://github.com/code-dot-org/code-dot-org) ។

<img alt="សិស្សនិងកុំព្យូទ័រយួរដៃ" src="/images/international/group_computer.jpg" width="100%" />

## តាម​យើង

[ ចុះឈ្មោះដើម្បីទទួលបានព័ត៌មានថ្មីៗ ](/about/hear-from-us) អំពីដំណើរការនៃការអភិវឌ្ឍនៃវិទ្យាសាស្ត្រកុំព្យូទ័រ K-12 និងអំពីការងាររបស់ Code.org ។ ឬតាមមើល Code.org នៅលើប្រព័ន្ធផ្សព្វផ្សាយសង្គម៖

{{ social_media_codeorg }}
