---
title: Tungkol
theme: responsive
video_player: true
---

# Tungkol Sa Amin

<div class="col-60">
  Ang Code.org® ay isang nonprofit na nakatuon sa pagpapalawak ng pag-access sa computer science sa mga paaralan at pagpapataas ng partisipasyon ng mga batang babae at mga mag-aaral mula sa ibang mga grupo na kulang sa representasyon. Ang aming pananaw ay ang bawat mag-aaral sa bawat paaralan na magkaroon ng pagkakataong matuto ng computer science, tulad ng biology, chemistry, o algebra. Nagbibigay kami ng pinakamalawak na ginagamit na kurikulum para sa pagtuturo ng computer science sa paaralang elementarya at sekondarya at inaayos din ang taunang <a href="https://hourofcode.com">Hour of Code</a> na kampanya, na may 10% ng lahat ng estudyante sa mundo. Ang Code.org ay sinusuportahan ng mga mapagbigay na donor kabilang ang Amazon, Facebook, Google, ang Infosys Foundation, Microsoft, at <a href="/about/donors">marami pa</a>.
</div>

[col-40]

<div style="background-color:#7665a0;height:190px;padding:25px;display:flex;justify-content:center;flex-direction:column">

  <font size="4" color="#FFFFFF"><b>Ang mga kurso sa Code.org ay ginagamit ng sampu-sampung milyong estudyante at ng isang milyong guro sa buong mundo.</b></font>

</div>

[/col-40]

<div style="clear:both"></div>

## Kasaysayan

Noong 2013, ang Code.org ay inilunsad ng magkakambal na sina Hadi at Ali Partovi na may [na video na nagsusulong ng computer science](https://www.youtube.com/watch?v=nKIu9yen5nc). Ang video na ito ay naging #1 sa YouTube sa loob ng isang araw, at 15,000 paaralan ang umabot sa amin para sa tulong. Simula noon, lumawak kami mula sa isang naka-bootstrap na tauhan ng mga boluntaryo para bumuo ng isang buong organisasyon na sumusuporta sa isang pandaigdigang kilusan. Naniniwala kami na ang isang de-kalidad na edukasyon sa computer science ay dapat na available sa bawat bata, hindi lamang ng masuwerteng iilan.
<br> <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/nKIu9yen5nc" frameborder="0" allowfullscreen></iframe>

## Ang Ginagawa Namin

Gumagawa kami sa buong saklaw ng edukasyon: pagdidisenyo ng [aming sariling mga kurso](https://studio.code.org/courses) o pakikipagsosyo sa iba, pagsasanay ng mga guro, pakikipagsosyo sa malalaking distrito ng paaralan, pagtulong sa pagbabago mga patakaran ng pamahalaan, pagpapalawak sa buong mundo sa pamamagitan ng pakikipagsosyo, at marketing para masira ang mga stereotype o estereotipo. Naitatayo ang aming trabaho dahil sa mga dekada ng pagsisikap, ng hindi mabilang na mga organisasyon at indibidwal na tumulong sa pagtatatag, pagpopondo, at pagpapalaganap ng edukasyon sa computer science. Nagpapasalamat kami sa walang sawang gawain ng mas malawak na komunidad ng edukasyon sa computer science, at nagpapasalamat kami sa lahat ng mga kasosyo at indibidwal na nagpalawak ng aming epekto sa mga nakaraang taon.

## Internasyonal na Pag-abot ng Code.org

Mahigit sa 40% ng aming trapiko sa website ay nagmumula sa labas ng United States at ang bilang na iyon ay patuloy na tumataas. Para mapalawak ang pandaigdigang pag-access sa computer science, ang aming pangkat ay nakikipagtulungan nang malapit sa [higit sa 100 internasyonal na mga kasosyo](/about/partners), na tinutulungan silang isulong ang Hour of Code, itaguyod ang pagbabago ng patakaran, at magsanay ng mga guro. Ginagawa namin ang computer science na bahagi ng internasyunal na diskurso sa edukasyon sa pamamagitan ng pakikipagsosyo sa mga ministeryo ng edukasyon mula sa buong mundo at pakikipagtulungan sa mga internasyonal na organisasyon tulad ng Organization for Economic Co-operation and Development at United Nations Educational, Scientific and Cultural Organization.

<img alt="Mga Internasyonal na Kasosyo ng Code.org" src="/images/international/international_partners.jpg" width="100%" />

## Pagsasalin

Gusto mo bang magkaroon ng direktang epekto sa mga mag-aaral sa iyong bansa? Tulungan kaming gawing available ang nilalaman ng Code.org sa kanilang sariling wika! Kung boluntaryo kang tumulong sa pagsasalin ng aming mga pagtuturo at mga aralin, makakasali ka sa isang komunidad ng higit sa 7,000 na mga tagasalin na tumutulong sa pagdadala ng edukasyon sa computer science sa mga mag-aaral sa buong mundo. Tingnan ang aming [translation guide](/translate) para sa karagdagang impormasyon.

## Ang aming pangako sa libreng kurikulum at open source na teknolohiya

Ang lahat ng mga mapagkukunan ng curriculum at mga pagtuturo na gagawin namin ay malayang magagamit at bukas na lisensyado sa ilalim ng lisensya ng [Creative Commons](http://creativecommons.org/licenses/by-nc-sa/4.0/) na lisensya, na nagpapahintulot sa iba na gumawa ng derivative na mapagkukunan ng edukasyon para sa mga di-komersyal na layunin. Kung interesado kang lisensyahan ang aming mga materyales para sa komersyal na layunin, [contact us](/contact). Ang aming mga kurso ay isinalin para sa pandaigdigang paggamit o ng mga nagsasalita ng iba't ibang wika. Ang aming teknolohiya ay binuo bilang isang [open source project](https://github.com/code-dot-org/code-dot-org).

<img alt="Mga mag-aaral na may laptop" src="/images/international/group_computer.jpg" width="100%" />

## I-follow kami

[Mag-sign up para makatanggap ng mga update sa katayuan](/about/hear-from-us) tungkol sa progreso ng K-12 computer science movement at tungkol sa gawain ng Code.org. O i-follow ang Code.org sa social media:

{{ social_media_codeorg }}
