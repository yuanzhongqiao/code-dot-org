---
title: Garis Panduan Aktiviti
layout: wide
---

<style>
  ul {
    margin: 0px 20px 20px 20px;
  }
</style>

# Garis panduan untuk mencipta dan menghantar tutorial dan pelajaran untuk Hour of Code™ (Jam Kod) dan Minggu Pendidikan Sains Komputer

Code.org menganjurkan pelbagai aktiviti, pelajaran dan video Hour of Code™ (Jam Kod) dalam laman web Code.org dan Jam Kod. Senarai semasa berada di [hourofcode.com/learn]({{ urls/learn }}).

Mahu menghantar tutorial pandu diri, pelajaran dipimpin guru, atau aktiviti robotik/pembuat yang menerangkan prinsip sains komputer? Sertailah pergerakan global ini dan bantu peserta di seluruh dunia memulakan dengan jam kod atau pergi lebih jauh dengan aktiviti pelbagai pelajaran, sepanjang hari, atau seminggu.

**Setelah membaca garis panduan, anda boleh menghantar aktiviti anda melalui [halaman Penghantaran Aktiviti Hour of Code™ (Jam Kod)](https://forms.gle/z8ARSLcJCFdNPVJS8) kami. Anda boleh menghantar aktiviti pada bila-bila masa, tetapi tarikh akhir kemasukan dalam mana-mana tahun kalendar yang tertentu ialah pada 1 Oktober. (Sebagai contoh, apa-apa aktiviti yang diterima selepas 1 Oktober, {{ campaign_date/year }} tidak akan disenaraikan untuk {{ campaign_date/year }} Jam Kod.) Tarikh akhir untuk menghantar semakan kualitatif awal [(butiran di bawah)](#earlyreview) ialah pada 16 Ogos.** Jika anda mempunyai apa-apa soalan tentang penghantaran aktiviti anda, sila hubungi kami di support@code.org.

Beberapa petua:

1. **Hantar lebih daripada satu aktiviti**: Jika anda telah membina aktiviti untuk pelbagai tahap, peringkat umur yang berbeza, atau kategori lain, kami akan menyenaraikan aktiviti anda secara berasingan supaya setiap guru boleh mencari aktiviti yang sesuai untuk kelas mereka. Hantarkan setiap tutorial atau aktiviti secara individu. Apabila dilihat bilangan penghantaran sejak beberapa tahun kebelakangan ini, kami akan mempunyai masa untuk menyemak **sehingga 5 aktiviti** untuk setiap rakan kongsi. Selepas itu, kami akan berusaha sebaik mungkin untuk menyemak sebanyak yang mungkin sebelum acara Jam Kod bermula.

2. **Di luar pelajaran individu yang baru bermula**: Di samping pelajaran untuk guru dan pelajar yang mempelajari sains komputer buat pertama kalinya, kami menyenaraikan pengalaman pembelajaran untuk kelas celik sains komputer yang mahu melangkah lebih jauh! Bantu kami dengan menghantar pelajaran untuk kelas yang sudah selesa dengan asas sains komputer.

3. **Bidang subjek**: Mempunyai idea pelajaran hebat yang mengintegrasikan Sains Komputer ke dalam Matematik? Sejarah? Seni Bahasa? Sains? Seni? Atau subjek lain? Kami telah menerima banyak permintaan daripada guru yang mahu menghubungkan Jam Kod dengan bidang subjek mereka. Guru boleh menyaring untuk jenis kelas mereka (kumpulan gred atau bidang subjek), oleh itu kami memerlukan bantuan anda mengisi jurang yang wujud bagi menawarkan aktiviti kelas atau rancangan pelajaran yang mengaitkan CS dengan setiap bidang subjek utama untuk kumpulan gred berbeza. Kami juga terus mengadakan kategori "Sains Komputer" untuk guru yang mencari aktiviti CS yang generik.

<a id="top"></a>

## Indeks:

- [Perkara yang boleh dihantar](#whatsubmit)
- [Garis panduan umum untuk mencipta aktiviti Hour of Code™ (Jam Kod)](#guidelines)
- [Cara Menghantar](#submit)
- [Cara aktiviti akan dinilaikan](#inclusion)
- [Garis panduan menghantar untuk semakan kualitatif awal](#design)
- [Cadangan untuk mereka bentuk aktiviti anda](#tm)
- [Garis Panduan Tanda Dagangan](#pixel)
- [Piksel Penjejak](#promote)

<a id="whatsubmit"></a>

## Perkara yang boleh dihantar

**Teka-teki atau permainan pandu diri ([contoh](https://code.org/dance))**

Aktiviti ini direka bentuk untuk pelajar mengarah diri melalui tutorial. Mereka tidak memerlukan banyak arahan daripada guru atau kerja penyediaan guru.

**Guru Mengendalikan pelajaran ([contoh](https://studio.code.org/s/course1/lessons/2/levels/1), [templat](https://docs.google.com/document/d/1DhaLNd6uim9rRPkHrglnOvVcgzGc8axKIzv8YI56JGA))**

Kini, ratusan ribu pendidik telah mencuba Jam Kod, banyak kelas bersedia untuk aktiviti lebih kreatif yang mengajar asas sains komputer. Untuk membantu guru yang lebih canggih mendapatkan inspirasi, kami mengumpul dan menguruskan pelajaran dan rancangan aktiviti "dipimpin guru" untuk veteran Jam Kod.

Satu jenis aktiviti yang akan kami paparkan untuk guru yang berpengalaman ialah projek penciptaan "sandbox terbuka". Aktiviti yang menggalakkan pelajar membina aplikasi, permainan, laman web atau projek lain mereka sendiri. Jika dikendalikan dengan betul, lebih banyak aktiviti terbuka boleh mempamerkan sifat kreatif sains komputer dengan lebih baik.

Sebilangan pendidik juga mungkin lebih suka menganjurkan aktiviti Jam Kod yang mengikuti format pelajaran tradisional berbanding pengalaman yang berpandukan teka-teki/permainan.

Anda boleh mulakan dengan [templat](https://docs.google.com/document/d/1DhaLNd6uim9rRPkHrglnOvVcgzGc8axKIzv8YI56JGA) ini untuk rancangan pelajaran anda.

**Aktiviti untuk guru dalam subjek/bidang lain**

Kami juga memaparkan rancangan pelajaran yang direka bentuk untuk bidang subjek berbeza. Sebagai contoh, rancangan pelajaran satu jam untuk mengajar kod di dalam kelas geometri. Atau latihan mad-lib untuk kelas Bahasa Inggeris. Atau aktiviti penciptaan kuiz kreatif untuk kelas sejarah. Ini boleh membantu merekrut guru dalam bidang subjek lain untuk membimbing aktiviti Jam Kod yang unik kepada bidangnya, sambil menunjukkan cara CS boleh mempengaruhi dan meningkatkan banyak bidang subjek berbeza.

Contoh:

- Imej Cermin (aktiviti untuk guru seni)
- Aktiviti arduino untuk guru fizik
- Sejarah aktiviti teknologi untuk guru sejarah
- Dan lihat [senarai ini](https://docs.google.com/document/d/19jQ2VhBUyIn30VFbfdHOVpmlymhNiB0AkEqRxHz3dr0/edit) untuk lebih banyak idea daripada pendidik (atau tambahkan idea anda sendiri ke dalam senarai untuk memberikan inspirasi kepada individu lain)

**Untuk pelajar yang memerlukan keperluan khas**

Jika anda mencipta aktiviti atau tutorial yang direka bentuk untuk pelajar yang memerlukan keperluan khas, sila nyatakan dalam huraian. Secara khususnya, terdapat sangat sedikit pilihan untuk individu yang cacat penglihatan. Jika aktiviti anda direka bentuk untuk hadirin ini, sila beritahu kami.

[**Kembali ke bahagian atas**](#top)

<a id="guidelines"></a>

## Garis panduan umum untuk mencipta aktiviti Jam Kod

Matlamat Jam Kod ialah memberikan kepada mereka yang baru bermula pengalaman pertama sains komputer atau pengaturcaraan yang mudah diakses. Nadanya hendaklah:

- Sesiapa sahaja boleh belajar sains komputer - tanpa mengira umur, jantina, bangsa, atau keupayaan/kecacatan.
- Sains komputer dihubungkan kepada pelbagai bidang dan minat yang luas. Semua individu perlu mempelajarinya!
- Galakkan pelajar untuk mencipta sesuatu yang unik yang dapat dikongsikan dengan ibu bapa/rakan atau dalam talian.

Aktivitinya mesti mengajar konsep sains komputer seperti gelung, bersyarat, penyulitan, atau cara Internet berfungsi. Aktiviti boleh juga mengajar tentang cara sains komputer menghubungkan pekerjaan, peristiwa, atau sejarah dunia sebenar. Sebagai contoh, mengajar reka bentuk UX untuk membuat aplikasi yang berguna kepada hadirin atau tujuan. Kita tidak menggalakkan aktiviti yang memberikan tumpuan kepada sintaks pengaturcaraan daripada konsep. Sebagai contoh, kami akan menyenaraikan, tetapi tidak menyoroti, aktiviti yang mengajar HTML. Begitu juga, kami tidak menggalakkan pelajaran pengaturcaraan blok yang memberikan tumpuan kepada menetapkan/mengubah pilihan konfigurasi daripada mempelajari cara memodelkan algoritma atau proses.

*Keperluan teknikal:* Ini kerana pelbagai jenis persediaan teknologi sekolah dan kelas, aktiviti terbaik ialah berasaskan Web atau mesra telefon pintar, atau sebaliknya aktiviti gaya nirplag yang mengajar konsep sains komputer tanpa penggunaan komputer (lihat <http://csunplugged.com/>). Aktiviti yang memerlukan pengalaman memasang aplikasi, aplikasi desktop, atau konsol permainan tidak menjadi masalah tetapi tidak sesuai. Kami tidak akan menyenaraikan aktiviti yang memerlukan pendaftaran atau bayaran. (Aktiviti robotik memerlukan pembelian robotik.)

*Format Dipimpin Pelajar (Pandu Diri):* Jam Kod asal kebanyakannya dibina berdasarkan kejayaan tutorial atau pelajaran pandu diri, secara pilihan dikendalikan oleh guru. Terdapat banyak pilihan sedia ada, tetapi jika anda mahu mencipta yang baharu, aktiviti ini harus direka bentuk supaya ia dapat memberikan keseronokan kepada pelajar yang melakukannya sendiri, atau di dalam kelas bahawa gurunya membuat persediaan atau mempunyai latar belakang CS yang minimum. Mereka perlu memberikan arahan kepada pelajar berbanding dengan cabaran terbuka selama sejam. Sebaik-baiknya, arahan dan tutorial diintegrasikan terus ke dalam platform pengaturcaraan, bagi mengelakkan beralih tab atau tetingkap antara tutorial dan platform pengaturcaraan.

Untuk mencuba pelbagai jenis tutorial dan rancangan pelajaran yang boleh anda cipta, sila kunjungi [halaman Aktiviti Jam Kod](https://hourofcode.com/us/learn).

[**Kembali ke bahagian atas**](#top)

<a id="submit"></a>

## Cara menghantar

Lawati [halaman Penghantaran Aktiviti Hour of Code™ (Jam Kod)](https://forms.gle/L1hjArNnGSMJPzyE8) dan lengkapkan soalan untuk menghantar aktiviti anda.

Perkara yang anda akan perlukan

* Nama dan e-mel hubungan utama yang mewakili aktiviti yang dihantar
* Nama aktiviti (tidak boleh memasukkan "Jam Kod" dalam namanya)
* Pautan URL kepada aktiviti tersebut
* Does this activity include AI: Yes or No
* An activity description (max character count: 400) 
    * Please include in the description whether it’s mainly student-guided or teacher-facilitated. Additionally, some schools are interested in knowing if Hour of Code activities address Common Core or Next Generation Science Standards. If the activity addresses specific standards, consider including this information.
* Recommended grade level(s) for intended users. You may refer to the \[Computer Science Teachers’ Association’s K-12 Standards\] (https://k12cs.org/framework-statements-by-grade-band/) for grade-appropriate computer science concepts. Example grade levels include: 
    * Kindergarten - Grade 1 (ages 4-6)
    * Grades 2-5 (ages 7-10)
    * Grades 6-8 (ages 11-13)
    * Grades 9+ (ages 14+)
* Senarai bahasa pengaturcaraan yang diajar oleh aktiviti anda. Iaitu C/C++, Java, JavaScript dan sebagainya. atau bebas bahasa (untuk rancangan pelajaran yang boleh diajar dalam pelbagai bahasa) choose language arts.
* Senarai bahasa semula jadi yang disokong. Catatan: Pengesanan bahasa ialah tanggungjawab penyedia aktiviti; kami akan mengarahkan semua pengguna pergi ke URL tunggal yang disediakan. or language independent (for lesson plans that can be taught in multiple languages)
* Apakah tahap pengalaman yang mesti dimiliki oleh pendidik untuk menggunakan aktiviti anda? (misalnya Baru Bermula atau Selesa.) Dan, apakah tahap pengalaman yang perlu dimiliki oleh pelajar?
* What level of experience should an educator have to use your activity? (e.g. Beginner or Comfortable.) And, what level of experience should the students have? If you’d like to prepare more advanced Hour of Code™ Activities, please include the prior knowledge needed in the description of your activity.
* The length of your activity 
    * 1 hour only
    * 1 hour with follow-on course
    * 2-6 hours (can be multiple lessons)
* A list of accessibility accommodations, if your activity has them. These include screen reader compatibility, text-to-speech capabilities, use of high-contrast colors, or any other accommodations made for learners with disabilities.

#### Perkara tambahan yang anda perlukan semasa menghantar Rancangan Pelajaran

* Pautan kepada rancangan pelajaran anda. Ini mungkin laman web, pautan dropbox, pemacu google atau perkhidmatan yang serupa.
* Apakah perisian dan/atau perkakasan yang diperlukan oleh guru untuk membuat rancangan pelajaran anda (Scratch? Robot? Tiada apa-apa?)

#### Perkara tambahan yang anda perlukan semasa menghantar Aktiviti Dalam Talian

* Nama dan logo organisasi anda
* sama ada ia mematuhi COPPA atau tidak
* pautan URL kepada nota guru (pilihan)
* Senarai platform yang diuji/serasi: 
    * Berasaskan web: Platform yang manakah telah anda uji? 
        * OS - Mac, Win dan versi lain
        * Pelayar - IE11, Edge, Firefox, Chrome, Safari
        * iOS mobile Safari (dioptimumkan untuk mudah alih)
        * Android Chrome (dioptimumkan untuk mudah alih)
    * Bukan berasaskan web: tentukan platform untuk kod natif (Mac, Win, iOS, Android, xBox dan yang lain)
    * Nirplag
* Syot layar atau imej pemasaran aktiviti Jam Kod. Sila hantarkan sekurang-kurangnya satu imej dengan dimensi 4:3. Imej ini sekurang-kurangnya 520 piksel didarabkan dengan 390 piksel. Imej ini TIDAK BOLEH mempunyai teks di atasnya (selain logo anda), untuk menjadikannya lebih mudah diakses oleh penutur bukan berbahasa Inggeris. Jika imej yang sesuai tidak disediakan, kami mungkin mengambil syot layar tutorial anda sendiri ATAU kami mungkin memilih untuk tidak menyenaraikannya. Semua imej mesti dihantar sebagai pautan URL kepada .jpg, .jpeg, atau .png.
* Untuk menjejaki penyertaan dengan lebih tepat, rakan kongsi tutorial pihak ketiga perlu memasukkan imej penjejak 1-piksel pada halaman pertama tutorial Jam Kod mereka. Lihat bahagian [Piksel Penjejak](#pixel) di bawah untuk maklumat lanjut.
* Setelah menyelesaikan aktiviti anda, pengguna perlu diarahkan kepada [code.org/api/hour/finish](https://code.org/api/hour/finish) apabila mereka akan dapat: 
    * Kongsikan di dalam media sosial bahawa mereka telah melengkapkan Jam Kod
    * Menerima sijil yang mereka telah melengkapkan Jam Kod
    * Lihat carta kedudukan tentang negara/bandar raya yang mempunyai kadar penyertaan tertinggi dalam aktiviti Jam Kod
    * Bagi pengguna yang menghabiskan sejam untuk aktiviti anda dan tidak melengkapkannya, sila sertakan butang pada aktiviti anda yang mengatakan "Saya sudah selesai dengan Jam Kod saya" yang menghubungkan kembali kepada [code.org/api/hour/finish](https://code.org/api/hour/finish) juga.
* (Pilihan) Kami mungkin melakukan tindakan susulan dengan tinjauan dalam talian/pautan borang yang meminta laporan metrik aktiviti berikut: 
    * Untuk aktiviti dalam talian (terutamanya aplikasi telefon pintar/tablet): 
        * Bilangan pengguna
        * Bilangan individu yang melengkapkan tugas
        * Purata masa bertugas
        * Bilangan jumlah barisan kod yang ditulis atas semua pengguna
        * Bilangan mereka yang meneruskan pembelajaran lanjut (dikira sebagai pengguna yang menyelesaikan tugas dan melakukan tugas tambahan di laman web anda)
    * Untuk aktiviti luar talian 
        * Bilangan muat turun aktiviti versi kertas (jika berkenaan)

#### Perkara tambahan yang anda perlukan semasa menghantar Robotik

* Jika anda menghantar aktiviti robotik, kami perlu mengetahui kos setiap pelajar untuk robotik
* Bagi menilai aktiviti robotik untuk disertakan dalam laman web, kami akan memerlukan anda menghantar kit contoh kepada penilai.

[**Kembali ke bahagian atas**](#top)

<a id="inclusion"></a>

## Cara aktiviti akan dinilaikan

Jawatankuasa pendidik sains komputer yang pelbagai akan menetapkan kedudukan berdasarkan kriteria kualitatif dan kuantitatif. Semua aktiviti yang sesuai dengan kriteria asas akan disenaraikan.

For traditional coding activities, a diverse committee of computer science educators will rank submissions based on qualitative and quantitative criteria. All activities that fit the basic criteria will be listed. Teachers will be able to filter and sort to find the best activities for their classroom.

Jika jawatankuasa penilaian mengadarkan aktiviti tersebut sebagai sifar dalam kualiti pengeluaran (disebabkan pepijat atau arahan yang teruk serta menjadikannya sangat sukar digunakan), dalam mempromosikan pembelajaran bagi kumpulan yang terpencil (disebabkan berkaitan dengan perkauman/seksis), dalam nilai pendidikan (tidak mengajar Konsep CS), atau menyeronokkan/menarik (disebabkan sukar/tawar hati untuk pelajar menyelesaikannya), aktiviti tersebut tidak akan disenaraikan.

- Kualiti pengeluaran yang tinggi
- Promosikan pembelajaran oleh semua kumpulan demografi (terutamanya pelajar yang terpencil dalam CS, seperti wanita muda, pelajar dari kumpulan kaum dan etnik yang terpinggir dan pelajar kurang upaya)
- Pendidikan (mengajar konsep sains komputer dengan baik)
- Menyeronokkan dan menarik
- Galakkan pelajar untuk mencipta sesuatu yang unik yang boleh mereka kongsikan (Untuk pelajar yang lebih muda: dengan ibu bapa dan rakan sekelas. Untuk pelajar yang lebih tua: di Internet)

Di samping itu, untuk disenaraikan, semua aktiviti mesti:

In addition, in order to be listed, all activities must:

- Wajar untuk kelas sekolah awam (tiada senjata api, tiada kandungan eksplisit/orang dewasa, tiada kandungan agama dan sebagainya)
- Tidak perlu mendaftar
- Tidak memerlukan bayaran (pengecualian untuk aktiviti robotik yang memerlukan pembelian robot/kit) Untuk aktiviti terarah kendiri untuk guru dan pelajar baharu, jawatankuasa penilaian akan mencari sama ada:

For self-directed activities for new teachers and students the review committee will be looking for whether:

- Terima penilaian tertinggi daripada jawatankuasa penilaian
- Aktiviti satu jam terarah kendiri direka bentuk untuk mereka yang baru bermula (pelajar dan guru)
- Menarik kepada pelbagai pengguna (di seluruh platform, bahasa dan peringkat umur)

Teachers and students will be able to search through and filter our list of activities based on filters such as grade, experience level, subject, hardware, etc. By default, we will show lesson plans and activities first that:

- Maklum balas positif - Apakah yang berfungsi dengan baik?
- Maklum balas kritikal - Apakah yang boleh diperbaiki?
- Appeal to a wide range of users (across platforms, languages, and ages)
- Adakah aktiviti ini menggalakkan pelajar untuk mencipta sesuatu yang unik?
- Are new this year

[**Kembali ke bahagian atas**](#top)

<a id="design"></a>

## Garis panduan menghantar untuk semakan kualitatif awal

Activities do not have to include AI, though we are looking to offer more AI related activities.

You can include the [Hour of Code logo](https://hourofcode.com/us/promote/resources#logo) in your tutorial, but this is not required. If you use the Hour of Code logo, see the trademark guidelines below. <u>Under no circumstances can the Code.org logo and name be used.</u> Both are trademarked, and can’t be co-mingled with a 3rd party brand name without express written permission.

Semua aktiviti yang dihantar untuk penilaian awal akan menerima maklum balas menjelang 1 September. Kami juga akan memberikan pautan kepada borang yang boleh anda gunakan untuk menerangkan perubahan khusus yang dibuat kepada aktiviti sebagai tindak balas untuk maklum balas.

Setiap aktiviti yang dihantar sebelum 1 Oktober akan menerima penilaian standard, sama ada ia menerima penilaian awal atau tidak. Semasa tempoh penilaian standard (September-Oktober), penilai akan memberikan tumpuan kepada penilaian aktiviti mengikut kriteria yang dihuraikan dalam bahagian "Cara aktiviti akan dinilaikan" di atas. Melainkan jika anda mengetahui perubahan khusus kepada aktiviti, penilai boleh dan kemungkinan akan bergantung pada maklum balas penilaian awal untuk menilaikan aktiviti.

- Apakah platform dan pelayar yang paling sesuai digunakan oleh tutorial?
- Adakah ia berfungsi pada telefon pintar? Tablet?
- Adakah anda menyarankan pengaturcaraan berpasangan?
- Pertimbangan untuk digunakan di dalam kelas? Misalnya, jika terdapat video, nasihatkan guru untuk menunjukkan video tersebut pada skrin tayangan supaya seluruh kelas dapat menonton bersama

**Incorporate feedback at the end of the activity.** (E.g. “You finished 10 levels and learned about loops! Great job!”)

**Encourage students to post to social media (where appropriate) when they've finished.** For example “I’ve done an Hour of Code with ________ Have you? #HourOfCode” or “I’ve done an #HourOfCode as a part of #CSEdWeek. Have you? @Scratch.” Use the hashtag **#HourOfCode** (with capital letters H, O, C)

**Create your activity in Spanish or in other languages besides English.**

**Explain or connect the activity to a socially significant context.** Computer programming becomes a superpower when students see how it can change the world for the better!

**Make sure your tutorial can be used in a [Pair Programming](http://www.ncwit.org/resources/pair-programming-box-power-collaborative-learning) paradigm.** This is particularly useful for the Hour of Code because many classrooms do not have 1:1 hardware for all students.

[**Kembali ke bahagian atas**](#top)

<a id="tm"></a>

## Cadangan untuk mereka bentuk tutorial pandu diri selama sejam

Hour of Code® and Hora del Código® are registered trademarks of Code.org. Many of our tutorial partners have used our "Hour of Code" trademarks on their web sites in connection with their Hour of Code activities. We don't want to prevent this usage, but we want to make sure the usage falls within a few limits:

1. Use “Hour of Code” only in connection with non-commercial CS Education activities in the context of the Hour of Code campaign, and for no other purpose.
2. Any reference to "Hour of Code" should be used in a fashion that doesn't suggest that it's your own brand name, but that it rather references the Hour of Code as a grassroots movement. Good example: "Participate in the Hour of Code ® at ACMECorp.com". Bad example: "Try Hour of Code by ACME Corp".
3. Use a “®” superscript in the most prominent places you mention "Hour of Code", both on your web site and in app descriptions.
4. Include language on the page (or in the footer), including links to the Hour of Code, CSEdWeek and Code.org web sites, that discloses both the following: a. Hour of Code® and Hora del Código® are registered trademarks of Code.org; and b. “The '[Hour of Code](http://hourofcode.com/) ® is a nationwide initiative by [Code.org](http://code.org/) to introduce millions of students to one hour of computer science and computer programming.”
5. Do not use "Hour of Code" in app names.
6. Do not use “Hour of Code” in connection with any commercial use or purpose (e.g., placing your Hour of Code activity behind a paywall; promoting another paid service as part of your Hour of Code activity; selling Hour of Code merchandise).
7. Do not use “Hour of Code” in connection with any activity that requires a login or account creation.

[**Kembali ke bahagian atas**](#top)

<a id="pixel"></a>

## Garis Panduan Tanda Dagangan

In order to more accurately track participation we ask every tutorial partner to include a 1-pixel tracking image on the first page of their Hour of Code tutorials. The pixel-image must be on the start page only. Do not include on any interim pages of your tutorial.

This will allow us to count users who do your Hour of Code tutorial. It will lead to more accurate participation counts for your tutorial.

If your tutorial is approved and included on the final tutorial page, Code.org will provide you with a unique tracking pixel for you to integrate into your tutorial. See example below.

NOTE: this isn't important to do for installable apps (iOS/Android apps, or desktop-install apps)

Example tracking pixels for Dance Party:

IMG SRC = <http://code.org/api/hour/begin_dance.png>   


[**Kembali ke bahagian atas**](#top)

<a id="promote"></a>

## Piksel Penjejak

Please promote your activity to your network! Direct them to your Hour of Code page. Your users are much more likely to react to a mailing from you about your activity. Use the international Hour of Code campaign during Computer Science Education Week as an excuse to encourage users to invite others to join in, and help us reach more students!

- Paparkan Jam Kod and CSEdWeek pada laman web anda. Contoh: <http://www.tynker.com/hour-of-code>
- Promosikan Jam Kod dengan menggunakan media sosial, media tradisional, senarai e-mel dan sebagainya, gunakan tanda pagar #HourOfCode (dengan huruf besar H, O, C)
- Anjurkan acara tempatan atau minta pekerja anda menganjurkan acara di sekolah atau kumpulan komuniti tempatan.
- Lihat [kit sumber](https://hourofcode.com/us/promote) kami untuk maklumat lanjut.

[**Kembali ke bahagian atas**](#top)

{{ signup_button }}