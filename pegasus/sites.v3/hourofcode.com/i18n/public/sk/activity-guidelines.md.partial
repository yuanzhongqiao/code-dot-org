---
title: Výukový návod
layout: wide
---

<style>
  ul {
    margin: 0px 20px 20px 20px;
  }
</style>

# Usmernenia na vytváranie a odosielanie aktivít a lekcií pre Hodinu kódu a Týždeň informatiky

Na webových stránkach Code.org a Hodina kódu™ sa nachádzajú rôzne aktivity, lekcie a videá pre Hodinu kódu. Aktuálny zoznam materiálov sa nachádza na adrese [hourofcode.com/learn]({{ urls/learn }}).

Chcete poslať svoj vlastnú samostatnú aktivitu, aktivitu vedenú učiteľom alebo aktivitu v oblasti robotiky či tvorby, ktorá vysvetľuje nejaký princíp v oblasti informatiky? Pripojte sa k tomuto celosvetovému hnutiu a pomôžte účastníkom na celom svete začať s Hodinou kódu alebo sa pokročiť ďalej s niekoľkohodinovými, celodennými alebo týždennými aktivitami.

**Po prečítaní pokynov môžete svoju aktivitu odoslať prostredníctvom našej [stránky na odosielanie aktivít pre Hodinu kódu](https://forms.gle/z8ARSLcJCFdNPVJS8). Aktivitu môžete odoslať kedykoľvek, ale termín na zaradenie do daného kalendárneho roka je 1. október. (Napríklad aktivity prijaté po 1. októbri 2022 nebudú zaradené do zoznamu Hodiny kódu na rok 2022.) Termín na odoslanie na predbežné kvalitatívne posúdenie [(podrobnosti nižšie)](#earlyreview) je 16. august.** Ak máte akékoľvek otázky týkajúce sa odoslania vašej aktivity, obráťte sa na nás na adrese support@code.org.

Niekoľko tipov:

1. **Pošlite viacero aktivít**: Ak ste vytvorili aktivity pre rôzne úrovne, rôzne vekové kategórie alebo iné skupiny, uvedieme vaše aktivity samostatne, aby si každý učiteľ mohol nájsť tú správnu pre svoju triedu. Každý kurz alebo aktivitu odošlite samostatne. Vzhľadom na počet prijatých návrhov v posledných rokoch budeme mať čas na preskúmanie **maximálne 5 aktivít ** na partnera. Potom sa budeme snažiť skontrolovať čo najviac z nich ešte pred spustením Hodiny kódu.

2. **Nielen pre začiatočníkov**: Okrem hodín pre učiteľov a žiakov, ktorí sa len začínajú učiť informatiku, uvádzame zoznam učebných pomôcok pre triedy, ktoré sa chcú v informatike posunúť o niečo ďalej! Pomôžte nám zaslaním lekcií pre triedy, ktoré už zvládajú základy.

3. **Predmety**: Máte skvelý nápad na vyučovaciu hodinu, ktorá spája informatiku s matematikou? Dejepisom? Jazykovedou? Prírodovedou? Výtvarnou výchovou? Iným predmetom? Dostali sme množstvo žiadostí od učiteľov, ktorí chcú prepojiť Hodinu kódu so svojím predmetom. Učitelia môžu filtrovať podľa typu triedy (ročník alebo vyučovací predmet), takže potrebujeme vašu pomoc pri vypĺňaní medzier, aby sme mohli ponúknuť aktivity v triede alebo plány vyučovacích hodín, ktoré prepájajú informatiku s každým hlavným vyučovacím predmetom pre rôzne ročníky. Naďalej máme aj kategóriu „Informatika“ pre učiteľov, ktorí hľadajú všeobecné aktivity v oblasti informatiky.

<a id="top"></a>

## Register:

- [Čo posielať](#whatsubmit)
- [Všeobecné pokyny na vytvorenie aktivity pre Hodinu kódu](#guidelines)
- [Ako posielať](#submit)
- [Ako sa budú aktivity hodnotiť](#inclusion)
- [Usmernenia na predloženie žiadosti o predbežné kvalitatívne posúdenie](#design)
- [Odporúčania pre vypracovanie vašej aktivity](#tm)
- [Pokyny pre ochranné známky](#pixel)
- [Sledovací pixel](#promote)

<a id="whatsubmit"></a>

## Čo posielať

**Samostatná hádanka alebo hra ([príklad](https://code.org/dance))**

Cieľom týchto aktivít je, aby žiaci sami prechádzali kurzom. Nevyžadujú veľa pokynov od učiteľa ani prípravnú prácu učiteľa.

**Hodina vedená učiteľom ([príklad](https://studio.code.org/s/course1/lessons/2/levels/1), [šablóna](https://docs.google.com/document/d/1DhaLNd6uim9rRPkHrglnOvVcgzGc8axKIzv8YI56JGA))**

Teraz, keď už státisíce pedagógov vyskúšali Hodinu kódu, sú mnohé triedy pripravené na ďalšie kreatívne aktivity, pri ktorých sa učia základy informatiky. Aby sme pokročilejším učiteľom pomohli nájsť inšpiráciu, zhromažďujeme a vyberáme „učiteľské“ lekcie a plány aktivít pre veteránov Hodiny kódu.

Jedným z typov aktivít, ktoré ponúkneme skúseným učiteľom, sú projekty „otvoreného pieskoviska“. Sú to aktivity, ktoré nabádajú žiakov k vytvoreniu vlastnej aplikácie, hry, webovej stránky alebo iného projektu. Pri správnom vedení môžu otvorenejšie aktivity lepšie poukázať na tvorivú povahu informatiky.

Niektorí pedagógovia môžu tiež uprednostniť organizovanie aktivít v rámci Hodiny kódu, ktoré sa riadia tradičným formátom vyučovacej hodiny, a nie formou riadených hádaniek/hier.

Môžete začať s touto [šablónou](https://docs.google.com/document/d/1DhaLNd6uim9rRPkHrglnOvVcgzGc8axKIzv8YI56JGA) pre váš plán hodiny.

**Aktivity pre učiteľov iných predmetov/odborov**

Ponúkame aj plány vyučovacích hodín určené pre rôzne predmety. Napríklad hodinový plán vyučovacej hodiny na vyučovanie kódu na hodine geometrie. Alebo cvičenie Mad Lib pre hodinu angličtiny. Prípadne kreatívna aktivita na tvorbu kvízov na hodine dejepisu. Tie môžu pomôcť pri získavaní učiteľov iných predmetov, aby uskutočnili aktivitu z Hodiny kódu, ktorá je jedinečná pre ich odbor, a zároveň ukázať, ako informatika môže ovplyvniť a rozšíriť mnohé rôzne predmety.

Príklady:

- Zrkadlové obrazy (aktivita pre učiteľa výtvarnej výchovy)
- Aktivita s Arduinom pre učiteľa fyziky
- Aktivita o dejinách technológií pre učiteľa dejepisu
- A pozrite si aj [tento zoznam](https://docs.google.com/document/d/19jQ2VhBUyIn30VFbfdHOVpmlymhNiB0AkEqRxHz3dr0/edit), kde nájdete ďalšie nápady od pedagógov (alebo pridajte do zoznamu svoje vlastné, aby ste inšpirovali ostatných).

**Pre žiakov so špeciálnymi potrebami**

Ak vytvoríte aktivitu alebo kurz, ktorý je určený pre žiakov so špeciálnymi potrebami, uveďte to v popise. Najmä pre osoby so zrakovým postihnutím existuje len veľmi málo možností. Ak je vaša aktivita určená pre toto publikum, dajte nám vedieť.

[**Späť na začiatok**](#top)

<a id="guidelines"></a>

## Všeobecné pokyny na vytvorenie aktivity pre Hodinu kódu

Cieľom Hodiny kódu je poskytnúť začiatočníkom prvé skúsenosti s informatikou alebo programovaním. Tón by mal byť takýto:

- Informatiku sa môže učiť každý - bez ohľadu na vek, pohlavie, rasu alebo schopnosti/zdravotné postihnutie.
- Informatika je spojená so širokou škálou oblastí a záujmov. Mal by sa ju naučiť každý!
- Nabádajte žiakov, aby vytvorili niečo jedinečné, o čo sa môžu podeliť s rodičmi/priateľmi alebo na internete.

Aktivity by mali slúžiť na výučbu konceptov informatiky, ako sú cykly, podmienky, šifrovanie alebo fungovanie internetu. Aktivita môže tiež poskytnúť informácie o tom, ako informatika súvisí s povolaniami, udalosťami alebo históriou v skutočnom svete. Napríklad výučba dizajnu používateľského prostredia s cieľom vytvárať aplikácie, ktoré majú zmysel pre publikum alebo vec. Neodporúčame aktivity, ktoré sa zameriavajú na syntax programovania, a nie na koncepty. Napríklad zaradíme, ale nezvýrazníme aktivity, ktoré vyučujú jazyk HTML. Podobne neodporúčame hodiny blokového programovania, ktoré sa zameriavajú na nastavovanie/zmenu konfiguračných možností namiesto výučby modelovania algoritmov alebo procesov.

*Technické požiadavky:* Vzhľadom na širokú škálu technologických vybavení škôl a tried sú najlepšie aktivity založené na webovom rozhraní alebo použiteľné na smartfóne či inak odpojené aktivity, ktoré sa zameriavajú na výučbu konceptov informatiky bez použitia počítača (pozrite si <http://csunplugged.com/>). Aktivity, ktoré si vyžadujú inštaláciu aplikácie, aplikáciu na počítači alebo skúsenosti s hrou na konzole, sú v poriadku, ale nie sú ideálne. Nezaradíme aktivity, ktoré si vyžadujú registráciu alebo platbu. (Robotické aktivity môžu vyžadovať zakúpenie robotických zariadení.)

*Formát aktivít vedených žiakom (samostatné):* Pôvodná Hodina kódu bola postavená najmä na úspechu samostatných výučbových aktivít alebo lekcií, ktoré prípadne viedol učiteľ. Na výber je množstvo existujúcich možností, ale ak chcete vytvoriť novú, tieto aktivity by mali byť navrhnuté tak, aby boli zábavné pre žiaka, ktorý pracuje sám, alebo v triede, ktorej učiteľ má minimálne skúsenosti s prípravou alebo informatikou. Na rozdiel od otvorenej hodinovej úlohy by mali žiakom poskytnúť pokyny. Ideálne je, ak sú návody a kurzy integrované priamo do programovacej platformy, aby sa zabránilo prepínaniu kariet alebo okien medzi kurzom a programovacou platformou.

Ak chcete získať predstavu o širokej škále typov kurzov a plánov vyučovacích hodín, ktoré môžete vytvoriť, navštívte stránku [Aktivity Hodiny kódu](https://hourofcode.com/us/sk/learn).

[**Späť na začiatok**](#top)

<a id="submit"></a>

## Ako posielať

Svoju aktivitu môžete odoslať vyplnením dotazníka na stránke [Hour of Code™ Activity Submission](https://forms.gle/z8ARSLcJCFdNPVJS8).

Čo budete potrebovať:

* Meno a e-mail hlavnej kontaktnej osoby zastupujúcej odosielanú aktivitu
* Názov aktivity (názov nesmie obsahovať výraz „Hour of Code“)
* Odkaz na aktivitu
* Does this activity include AI: Yes or No
* An activity description (max character count: 400) 
    * Please include in the description whether it’s mainly student-guided or teacher-facilitated. Additionally, some schools are interested in knowing if Hour of Code activities address Common Core or Next Generation Science Standards. If the activity addresses specific standards, consider including this information.
* Recommended grade level(s) for intended users. You may refer to the \[Computer Science Teachers’ Association’s K-12 Standards\] (https://k12cs.org/framework-statements-by-grade-band/) for grade-appropriate computer science concepts. Example grade levels include: 
    * Kindergarten - Grade 1 (ages 4-6)
    * Grades 2-5 (ages 7-10)
    * Grades 6-8 (ages 11-13)
    * Grades 9+ (ages 14+)
* Zoznam programovacích jazykov, ktoré sa v rámci vašej aktivity vyučujú. Napríklad: C/C++, Java, JavaScript atď. alebo jazykovo nezávislé (pre plány hodín, ktoré možno vyučovať vo viacerých jazykoch) choose language arts.
* Zoznam podporovaných prirodzených jazykov. Poznámka: Za zisťovanie jazyka je zodpovedný poskytovateľ aktivity; všetkých používateľov presmerujeme na jedinú poskytnutú adresu URL. or language independent (for lesson plans that can be taught in multiple languages)
* Akú úroveň skúseností by mal mať pedagóg, aby mohol využiť vašu aktivitu? (napr. začiatočník alebo pokročilý). A akú úroveň skúseností by mali mať žiaci?
* What level of experience should an educator have to use your activity? (e.g. Beginner or Comfortable.) And, what level of experience should the students have? If you’d like to prepare more advanced Hour of Code™ Activities, please include the prior knowledge needed in the description of your activity.
* The length of your activity 
    * 1 hour only
    * 1 hour with follow-on course
    * 2-6 hours (can be multiple lessons)
* A list of accessibility accommodations, if your activity has them. These include screen reader compatibility, text-to-speech capabilities, use of high-contrast colors, or any other accommodations made for learners with disabilities.

#### Ďalšie veci, ktoré budete potrebovať pri odosielaní plánov hodín

* Odkaz na váš plán hodiny. Môže to byť webová stránka, odkaz na umiestnenie v DropBox, na Disku Google alebo v podobnej službe.
* Aký softvér alebo hardvér bude učiteľ potrebovať na realizáciu vášho plánu vyučovacej hodiny (Scratch? Robots? Žiadny?)

#### Ďalšie veci, ktoré budete potrebovať pri odosielaní online aktivít

* Názov a logo vašej organizácie
* Či je v súlade so zákonom COPPA alebo nie
* Odkaz na poznámky pre učiteľa (nepovinné)
* Zoznam testovaných/kompatibilných platforiem: 
    * Webové: Ktoré platformy ste testovali? 
        * Operačný systém: MacOS, Windows a ďalšie verzie
        * Prehliadače: IE11, Edge, Firefox, Chrome, Safari
        * Safari pre iOS (optimalizované pre mobily)
        * Chrome pre Android (optimalizované pre mobily)
    * Nie webové: určte platformu pre natívny kód (MacOS, Windows, iOS, Android, xBox alebo inú)
    * Unplugged
* Snímka obrazovky alebo marketingový obrázok aktivity pre Hodinu kódu. Pošlite aspoň jeden obrázok s pomerom strán 4:3. Rozlíšenie by malo byť aspoň 520 × 390 pixelov. Na tomto obrázku by nemal byť ŽIADNY text (okrem vášho loga), aby bol zrozumiteľnejší pre používateľov, ktorí neovládajú angličtinu. Ak neposkytnete vhodný obrázok, môžeme si urobiť vlastnú snímku obrazovky vašej aktivity ALEBO sa môžeme rozhodnúť, že ju neuvedieme na stránke. Všetky obrázky sa musia odoslať ako odkaz na súbor .jpg, .jpeg alebo .png.
* V záujme presnejšieho sledovania účasti musia externí partneri, ktorí poskytujú kurzy, na prvú stránku svojich kurzov v rámci Hodiny kódu umiestniť jednopixelové sledovacie obrázky. Viac informácií nájdete v časti [Sledovací pixel](#pixel) nižšie.
* Po ukončení vašej aktivity by sa používateľom mala zobraziť stránka [code.org/api/hour/finish](https://code.org/api/hour/finish), kde budú môcť: 
    * zdieľať na sociálnych sieťach informáciu o absolvovaní Hodiny kódu;
    * získať certifikát o absolvovaní Hodiny kódu;
    * pozrieť si rebríčky krajín/miest s najvyššou mierou účasti na aktivitách Hodiny kódu.
    * Pre používateľov, ktorí strávia hodinu vašou aktivitou a nedokončia ju, vložte do svojej aktivity tlačidlo s nápisom „Skončil som s Hodinou kódu“, ktoré tiež odkazuje na stránku [code.org/api/hour/finish](https://code.org/api/hour/finish).
* (Nepovinné) Môžeme následne zaslať odkaz na online prieskum/formulár, v ktorom požiadame o poskytnutie prehľadu o týchto ukazovateľoch aktivity: 
    * Pre online aktivity (najmä aplikácie pre smartfóny/tablety): 
        * počet používateľov,
        * koľko ľudí dokončilo úlohu,
        * priemerný čas riešenia úlohy,
        * celkový počet riadkov kódu napísaných všetkými používateľmi,
        * koľko používateľov pokračovalo v ďalšom vzdelávaní (merané ako každý používateľ, ktorý dokončil úlohu a pokračoval v ďalších úlohách na vašej stránke).
    * Pre offline aktivity 
        * počet stiahnutí papierovej verzie aktivity (ak sa uplatňuje).

#### Ďalšie veci, ktoré budete potrebovať pri odosielaní úloh pre robotiku

* Ak predložíte aktivitu v oblasti robotiky, potrebujeme poznať náklady na jedného žiaka.
* Na vyhodnotenie aktivít zameraných na robotiku na účely ich zaradenia na webovú stránku budeme potrebovať, aby ste recenzentom poslali vzorové modely.

[**Späť na začiatok**](#top)

<a id="inclusion"></a>

## Ako sa budú aktivity hodnotiť

Rôznorodá komisia zložená z učiteľov informatiky vyhodnotí predložené materiály na základe kvalitatívnych a kvantitatívnych kritérií. Zverejnia sa všetky aktivity, ktoré spĺňajú základné kritériá.

For traditional coding activities, a diverse committee of computer science educators will rank submissions based on qualitative and quantitative criteria. All activities that fit the basic criteria will be listed. Teachers will be able to filter and sort to find the best activities for their classroom.

Ak hodnotiaca komisia ohodnotí aktivitu nulou z hľadiska kvality produkcie (kvôli závažným chybám alebo pokynom, ktoré veľmi sťažujú jej používanie), z hľadiska podpory vzdelávania v nedostatočne zastúpených skupinách (kvôli rasistickému/sexistickému materiálu), z hľadiska vzdelávacej hodnoty (nevysvetľuje koncepty informatiky) alebo zábavnosti/pútavosti (kvôli tomu, že je pre žiakov náročná/odrádzajúca), aktivita nebude zaradená.

- Vysoká kvalita produkcie
- Podpora vzdelávania všetkých demografických skupín (najmä žiakov nedostatočne zastúpených v informatike, ako sú mladé ženy, žiaci z marginalizovaných rasových a etnických skupín a žiaci so zdravotným postihnutím)
- Vzdelávacia hodnota (dobre vysvetľuje jednotlivé koncepty z oblasti informatiky)
- Zábavnosť a pútavosť
- Podnecovanie žiakov k tomu, aby vytvorili niečo jedinečné, o čo sa môžu podeliť (v prípade mladších žiakov: s rodičmi a spolužiakmi, v prípade starších žiakov: na internete)

Okrem toho, aby mohli byť všetky aktivity zaradené do zoznamu, musia spĺňať tieto kritériá:

In addition, in order to be listed, all activities must:

- musia byť vhodné pre vyučovanie na verejnej škole (žiadne zbrane, žiadny explicitný obsah/obsah pre dospelých, žiadny náboženský obsah atď.);
- nesmú vyžadovať registráciu;
- nesmú vyžadovať žiadnu platbu (výnimkou sú aktivity v oblasti robotiky, ktoré môžu vyžadovať nákup robota/modelu) Pri samostatne vedených aktivitách pre nových učiteľov a žiakov bude hodnotiaca komisia skúmať, či:

For self-directed activities for new teachers and students the review committee will be looking for whether:

- získajú najvyššie hodnotenie od hodnotiacej komisie;
- sú jednohodinové samostatne vedené aktivity určené pre začiatočníkov (žiakov a učiteľov);
- oslovujú širokú škálu používateľov (naprieč platformami, jazykmi a vekovými kategóriami);

Teachers and students will be able to search through and filter our list of activities based on filters such as grade, experience level, subject, hardware, etc. By default, we will show lesson plans and activities first that:

- Pozitívna spätná väzba – Čo sa podarilo?
- Kritická spätná väzba – Čo by sa dalo zlepšiť?
- Appeal to a wide range of users (across platforms, languages, and ages)
- Podnecuje táto aktivita žiakov k tomu, aby vytvorili niečo jedinečné?
- Are new this year

[**Späť na začiatok**](#top)

<a id="design"></a>

## Usmernenia na predloženie žiadosti o predbežné kvalitatívne posúdenie

Activities do not have to include AI, though we are looking to offer more AI related activities.

You can include the [Hour of Code logo](https://hourofcode.com/us/promote/resources#logo) in your tutorial, but this is not required. If you use the Hour of Code logo, see the trademark guidelines below. <u>Under no circumstances can the Code.org logo and name be used.</u> Both are trademarked, and can’t be co-mingled with a 3rd party brand name without express written permission.

Všetky aktivity predložené na predbežné posúdenie dostanú spätnú väzbu do 1. septembra. Poskytneme vám aj odkaz na formulár, ktorý môžete použiť na vysvetlenie konkrétnych zmien vykonaných v aktivite v reakcii na spätnú väzbu.

Každá aktivita predložená pred 1. októbrom bude štandardne posúdená bez ohľadu na to, či bola alebo nebola posúdená predbežne. Počas obdobia štandardného posudzovania (september – október) sa hodnotitelia zamerajú na hodnotenie aktivít podľa kritérií opísaných v časti „Ako sa budú aktivity hodnotiť“ vyššie. Pokiaľ nezaznamenáte konkrétne zmeny v aktivite, hodnotitelia sa pri hodnotení aktivít môžu a pravdepodobne aj budú spoliehať na spätnú väzbu z prvotného posúdenia.

- Na akých platformách a v akých prehliadačoch funguje tento kurz najlepšie?
- Funguje na smartfónoch? Tabletoch?
- Odporúčate párové programovanie?
- Čo je potrebné zvážiť pri používaní v triede? Napr. ak sú k dispozícii videá, odporučte učiteľom, aby ich premietli na plátno, aby si ich mohla pozrieť celá trieda.

**Incorporate feedback at the end of the activity.** (E.g. “You finished 10 levels and learned about loops! Great job!”)

**Encourage students to post to social media (where appropriate) when they've finished.** For example “I’ve done an Hour of Code with ________ Have you? #HourOfCode” or “I’ve done an #HourOfCode as a part of #CSEdWeek. Have you? @Scratch.” Use the hashtag **#HourOfCode** (with capital letters H, O, C)

**Create your activity in Spanish or in other languages besides English.**

**Explain or connect the activity to a socially significant context.** Computer programming becomes a superpower when students see how it can change the world for the better!

**Make sure your tutorial can be used in a [Pair Programming](http://www.ncwit.org/resources/pair-programming-box-power-collaborative-learning) paradigm.** This is particularly useful for the Hour of Code because many classrooms do not have 1:1 hardware for all students.

[**Späť na začiatok**](#top)

<a id="tm"></a>

## Odporúčania na vytvorenie jednohodinových samostatných kurzov

Hour of Code® and Hora del Código® are registered trademarks of Code.org. Many of our tutorial partners have used our "Hour of Code" trademarks on their web sites in connection with their Hour of Code activities. We don't want to prevent this usage, but we want to make sure the usage falls within a few limits:

1. Use “Hour of Code” only in connection with non-commercial CS Education activities in the context of the Hour of Code campaign, and for no other purpose.
2. Any reference to "Hour of Code" should be used in a fashion that doesn't suggest that it's your own brand name, but that it rather references the Hour of Code as a grassroots movement. Good example: "Participate in the Hour of Code ® at ACMECorp.com". Bad example: "Try Hour of Code by ACME Corp".
3. Use a “®” superscript in the most prominent places you mention "Hour of Code", both on your web site and in app descriptions.
4. Include language on the page (or in the footer), including links to the Hour of Code, CSEdWeek and Code.org web sites, that discloses both the following: a. Hour of Code® and Hora del Código® are registered trademarks of Code.org; and b. “The '[Hour of Code](http://hourofcode.com/) ® is a nationwide initiative by [Code.org](http://code.org/) to introduce millions of students to one hour of computer science and computer programming.”
5. Do not use "Hour of Code" in app names.
6. Do not use “Hour of Code” in connection with any commercial use or purpose (e.g., placing your Hour of Code activity behind a paywall; promoting another paid service as part of your Hour of Code activity; selling Hour of Code merchandise).
7. Do not use “Hour of Code” in connection with any activity that requires a login or account creation.

[**Späť na začiatok**](#top)

<a id="pixel"></a>

## Pokyny pre ochranné známky

In order to more accurately track participation we ask every tutorial partner to include a 1-pixel tracking image on the first page of their Hour of Code tutorials. The pixel-image must be on the start page only. Do not include on any interim pages of your tutorial.

This will allow us to count users who do your Hour of Code tutorial. It will lead to more accurate participation counts for your tutorial.

If your tutorial is approved and included on the final tutorial page, Code.org will provide you with a unique tracking pixel for you to integrate into your tutorial. See example below.

NOTE: this isn't important to do for installable apps (iOS/Android apps, or desktop-install apps)

Example tracking pixels for Dance Party:

IMG SRC = <http://code.org/api/hour/begin_dance.png>   


[**Späť na začiatok**](#top)

<a id="promote"></a>

## Sledovací pixel

Please promote your activity to your network! Direct them to your Hour of Code page. Your users are much more likely to react to a mailing from you about your activity. Use the international Hour of Code campaign during Computer Science Education Week as an excuse to encourage users to invite others to join in, and help us reach more students!

- Prezentujte Hodinu kódu a CSEdWeek na svojich webových stránkach. Napríklad: <http://www.tynker.com/hour-of-code>.
- Propagujte Hodinu kódu prostredníctvom sociálnych médií, tradičných médií, poštových zoznamov atď. s použitím hashtagu #HourOfCode (s veľkými písmenami H, O, C).
- Usporiadajte miestne podujatie alebo požiadajte svojich zamestnancov, aby zorganizovali podujatie v miestnych školách alebo komunitných skupinách.
- Ďalšie informácie nájdete v našej [súprave zdrojov](https://hourofcode.com/us/promote).

[**Späť na začiatok**](#top)

{{ signup_button }}