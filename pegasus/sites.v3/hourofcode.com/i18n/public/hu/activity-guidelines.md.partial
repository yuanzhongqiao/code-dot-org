---
title: Közösségi iránymutatások
layout: wide
---

<style>
  ul {
    margin: 0px 20px 20px 20px;
  }
</style>

# Útmutató a Kódolás órája™ és a Számítástechnikai Oktatási Hét eseményeihez

A Code.org, a Hour of Code és CSEdWeek webhelyeken különféle, a Kódolás Órájához™ tartozó tevékenységeket, feladatokat és videókat találhattok. A legfrissebb lista az alábbi linken érhető el: [hourofcode.com/learn]({{ urls/learn }}).

A Code.org különféle Kódolás órája™ tevékenységeket, oktatatóanyagokat és videókat tartalmaz a Code.org, a Hour of Code és a CSEdWeek webhelyeken. A jelenlegi lista a [ hourofcode.com/learn ]({{ urls/learn }}) címen található.

Szeretnéd elküldeni saját önálló játékodat, esetleg tanóra tervedet vagy egyéb tevékenységet, amely segít elmagyarázni a számítástechnika alapelvét? Csatlakozz ehhez a globális mozgalomhoz, és segítsd a világ résztvevőit ösztönözni arra, hogy kezdjenek neki a kódolásnak, akár egy órás vagy akár hosszabb időtartamú - több órás, napos vagy hetes - tevékenységek kipróbálásával. (Az október 1-je után beküldött feladatok már nem kapnak helyet a Kódolás Órája listájában.) Az időben elvégzett minőségi felülvizsgálat érdekében [(a részletek alább) ](#earlyreview) a benyújtás határideje augusztus 16. </strong> Ha bármilyen kérdése van a tevékenység benyújtásával kapcsolatban, kérjük, forduljon hozzánk az alábbi címen: a support@code.org

Néhány tipp:

1. **Több mint egy ötlet benyújtása**: Ha különböző szintű, különböző korú vagy más kategóriákhoz tartozó tevékenységeket készítettél, akkor ezeket kérlek külön sorold fel, így minden tanár megtalálhatja az osztályához legjobban passzolót. Tehát ha korábban már létrehoztál egy weboldalt, és egyetlen URL-t küldtél meg nekünk a reklámozáshoz, akkor javasoljuk, hogy a jövőben ezt inkább kicsit máshogy csináld. Minden oktatóprogramot, ötletet külön-külön nyújts be nekünk. Tekintettel az elmúlt években beküldött nagyszámú ötletáradatra, a jövőben partnereként csak 5 tevékenység ötletet tudunk felülvizsgálni.

2. ** A kezdeti tanórákon túlmenően**: Azokon az órákon kívül, amelyek a számítástechnikával első alkalommal ismerkedni vágyó tanároknak és diákoknak szólnak, vannak olyan oktatóanyagaink is, amelyek már a programozás iránt komolyabban érdeklődőkre fókuszálnak, akik szeretnének továbblépni az első osztálytermi élményt követően! Segíts nekünk olyan tanóra-tervek benyújtásában, amelyek már az alap információkra támaszkodva a következő lépcsőfokot tűzték ki célul.

3. ** Tantárgyi érintettség**: Van netán egy olyan ötleted, amely integrálja a matematika és a számítástechnika oktatást? Esetleg történelmet? Irodalmat? Tudományt? Művészetet? Vagy más tantárgyat? Számos kérést kaptunk olyan tanároktól, akik szeretnék a Kódolás óráját összekapcsolni egyes tudományterületekkel, tantárgyi anyagokkal (különösen a továbbfejlesztett ISTE-szabványokkal, beleértve az „innovatív tervezés” és a „számítógépes gondolkodás” témaköröket). Ahhoz, hogy a tanárok rá tudjanak keresni az egyes jellemzőkre (évfolyam vagy tantárgy) és ki tudják választani az osztályuknak legmegfelelőbbet, kérlek, segíts kitölteni a tevékenységekre vonatkozó részletesebb adatokat, úgy mint vonatkozó tantárgyi terület, évfolyam/korosztály. Továbbra is fenntartjuk a „Számítástechnika” kategóriát azoknak a tanároknak, akik általános számítástechnikai tevékenységeket keresnek.

<a id="top"></a>

## Tárgymutató:

- [Mit kell benyújtani](#whatsubmit)
- [Általános irányelvek a Kódolás órája™ tevékenységek létrehozásához](#guidelines)
- [Hogyan küldd el](#submit)
- [Hogyan lesznek értékelve az ötletek](#inclusion)
- [Javaslatok a tevékenységek megtervezéséhez](#design)
- [Védjegyre vonatkozó irányelvek](#tm)
- [Pixel követése](#pixel)
- [A tevékenységek, a Számítástechnikai Oktatási Hét és a Kódolás órája népszerűsítése](#promote)

<a id="whatsubmit"></a>

## Mit kell benyújtani

**Önállóan megoldható rejtvény vagy játék ([example](https://code.org/dance))**

Ezeket a tevékenységeket arra tervezték, hogy a diákokat önállóan kalauzolják végig egy-egy gyakorlaton. Nem igényelnek tanári közreműködést vagy sok előkészítő munkát.

**Tanár által levezényelt foglalkozás ([example](https://studio.code.org/s/course1/stage/2/puzzle/1), [template](https://docs.google.com/document/d/1DhaLNd6uim9rRPkHrglnOvVcgzGc8axKIzv8YI56JGA))**

Most, hogy már oktatók tízezrei kipróbálták a Kódolás Óráját, sok tanterem készen áll a kreatívabb tevékenységekre, amelyek bevezethetik a gyerekeket a számítástechnika alapjaiba. Annak érdekében, hogy a haladóbb szemléletű tanárok is inspirációt találhassanak, különböző "tanár által levezényelt órákat" és tevékenységeket gyűjtünk a Kódolás Órája veteránjainak.

Most, hogy már több tízezer oktató kipróbálta a Kódolás óráját, sok osztály készen áll az újabb izgalmakra, kreatív tevékenységekre, amelyek megtanítják számukra a számítástechnika alapjait. Annak érdekében, hogy a tapasztaltabb tanárok további inspirációt meríthessenek, összegyűjtjük a tanóra-terveket és gondoskodunk arról, hogy a Kódolás órája veteránjainak is újat tudjunk mutatni. Megfelelő előkészítés esetén a kevésbé keretek közé szorított tevékenységek sokkal inkább megmutatják a számítástechnika kreatív oldalát.

Vannak persze olyan oktatók, akik a Kódolás órája eseményt szeretnék inkább egy hagyományos tanóra mintájára megvalósítani.

Vannak persze olyan oktatók, akik a Kódolás órája eseményt szeretnék inkább egy hagyományos tanóra mintájára megvalósítani.

**Tevékenységek más tantárgyak / szakterületek oktatói számára**

A fentieken túl különböző tantárgyak számára is dolgoztunk ki óraterveket. Ezekből például kiderül, hogyan is zajlik egy óra programozás egy geometria órán. Vagy egy mad-lib gyakorlat egy magyar órán. Vagy netán kvíz-készítés történelem órán. Mindez segítséget nyújt azoknak a tanároknak, akik ugyan más tantárgyakat oktatnak, de szeretnének megrendezni egy Kódolás órája eseményt. A tantervek ráirányítják a figyelmet a szaktárgy jellemző területeire, segítenek a megértésében, és mindeközben bemutatják a számítástechnika fontosságát is.

Példák:

- Tükörképek (tanóraterv egy rajztanár számára)
- Arduino tevékenység bemutatása egy fizika tanár számára
- A technológia története egy történelem tanár számára
- Az alábbi [listában](https://docs.google.com/document/d/19jQ2VhBUyIn30VFbfdHOVpmlymhNiB0AkEqRxHz3dr0/edit) további ötleteket találsz más tanároktól (de te is hozzáadhatod a saját ötletedet, hogy másokat is inpirálhasson.)

**Speciális igényű hallgatók számára**

Ha olyan tevékenységet vagy oktatóanyagot hozol létre, amelyet speciális igényű hallgatóknak szántál, akkor ez alapján jelöld a listában, hogy mások is megtalálhassák. Ugyanis különösen nagy a hiány a látássérült diákok számára készült anyagokból. Amennyiben az általad létrehozott tanóraterv a speciális igényűeket célozza, kérlek, oszd meg velünk.

[**Vissza az elejére**](#top)

<a id="guidelines"></a>

## Általános irányelvek a Kódolás órája esemény létrehozásához

A Kódolás órájának célja, hogy közérthető módon mutassa be a számítástechnikát és a programozás világát mindazoknak, akik elsőként vágnak neki ennek az útnak. Fontosnak tartjuk kihangsúlyozni, hogy:

- Bárki tanulhat számítástechnikát - kortól, nemtől és bőrszíntől függetlenül.
- A számítástechnika számos területhez és érdeklődési körhöz kapcsolódik. Mindenkinek érdemes lenne megismerkednie vele!
- Bátorítsd te is diákjaidat, hogy hozzanak létre valami egyedit, amelyet megoszthatnak szüleikkel/barátaikkal, vagy közzétehetnek online.

A Kódolás órájának célja, hogy közérthető módon mutassa be a számítástechnikát és a programozás világát mindazoknak, akik elsőként vágnak neki ennek az útnak. Fontosnak tartjuk kihangsúlyozni, hogy: Erre példa a felhasználói felületek tervezésének oktatása is, amely olyan alkalmazások készítésére irányul, amelyek fontos szerepet töltenek be a mindennapi ember életében. Nem támogatjuk viszont azokat a tevékenységeket, amelyek a koncepcióalkotás helyett a programozás mondattanának elsajátítsára koncentrálnak. Például bár közzéteszük, de nem reklámozzuk azokat a tevékenységeket, amelyek a HTML nyelvet oktatják. Hasonlóképpen elutasítjuk azokat a tanóraterveket, amelyek a konfigurációs beállításokra vagy azok megváltoztatására összpontosítanak, nem pedig az algoritmus vagy a folyamat modellezésére.

Ezeken a tevékenységeken keresztül elsajátíthatók a számítástechnika alapfogalmai és elvei, mint például a ciklus, feltétel, titkosítás vagy az Internet működése. Egy-egy tevékenység azt is megtaníthatja, hogy a számítástechnika hogyan kapcsolódik a mindennapi élethez, egyes szakmákhoz, eseményekhez vagy akár a történelemhez. Erre példa a felhasználói felületek tervezésének oktatása is, amely olyan alkalmazások készítésére irányul, amelyek fontos szerepet töltenek be a mindennapi ember életében. Nem támogatjuk viszont azokat a tevékenységeket, amelyek a koncepcióalkotás helyett a programozás mondattanának elsajátítsára koncentrálnak.

* Technikai követelmények: * Mivel az iskolák jellemzően eltérő technológiai felszereltséggel rendelkeznek, a legnépszerűbb feladatokat web vagy mobil alapúra terveztük, illetve vannak olyanok, amelyek eszközök használata nélkül is megoldhatók.(További részletekért látogasd meg a[ http://csunplugged.com/](http://csunplugged.com/) weboldalt). Olyan tevékenységek, amelyek kiegészítő alkalmazások telepítését vagy játékkonzol használatát igénylik, nem a legideálisabbak. Azokat a tevékenységeket, amelyek regisztrációt vagy fizetést igényelnek, nem tesszük közzé. (A robotika tevékenységekhez robotikai kiegészítők vásárlása szükséges.)

Ha meg szeretnéd ismerni a létrehozható oktatóanyagok és óratervek sokféleségét, látogass el a [ A Kódolás Órája, Tevékenységek](https://hourofcode.com/us/learn) oldalra.

[**Vissza az elejére**](#top)

<a id="submit"></a>

## Hogyan kell benyújtani?

Látogasd meg a [ Kódolás órája™ tevékenységek benyújtása](http://bit.ly/29zt9ki) oldal, és töltsd ki a vonatkozó kérdéseket.

Amire szükséged lesz:

* Az elsődleges kapcsolattartó neve és e-mail címe, aki a benyújtott tevékenységet képviseli
* Tevékenység neve (nem tartalmazhatja a "Kódolás órája" elnevezést)
* URL link a tevékenységhez
* Does this activity include AI: Yes or No
* An activity description (max character count: 400) 
    * Please include in the description whether it’s mainly student-guided or teacher-facilitated. Additionally, some schools are interested in knowing if Hour of Code activities address Common Core or Next Generation Science Standards. If the activity addresses specific standards, consider including this information.
* Recommended grade level(s) for intended users. You may refer to the \[Computer Science Teachers’ Association’s K-12 Standards\] (https://k12cs.org/framework-statements-by-grade-band/) for grade-appropriate computer science concepts. Example grade levels include: 
    * Kindergarten - Grade 1 (ages 4-6)
    * Grades 2-5 (ages 7-10)
    * Grades 6-8 (ages 11-13)
    * Grades 9+ (ages 14+)
* A tevékenység által tanított programozási nyelvek listája. Például C/C++, Java, JavaScript, stb. vagy nyelvfüggetlen (több nyelven is tanítható óratervekhez) choose language arts.
* A támogatott természetes nyelvek listája. Megjegyzés: A nyelv kiválaszthatóságáért a tevékenység benyújtója felel; minden felhasználót a megadott egyetlen URL-re irányítunk át. or language independent (for lesson plans that can be taught in multiple languages)
* Milyen tapasztalatokkal kell rendelkeznie az oktatóknak a tevékenység elvégzéséhez? (Például kezdő vagy tapasztalt.) Továbbá, milyen szintű tapasztalatokkal kell rendelkeznie a diákoknak?
* What level of experience should an educator have to use your activity? (e.g. Beginner or Comfortable.) And, what level of experience should the students have? If you’d like to prepare more advanced Hour of Code™ Activities, please include the prior knowledge needed in the description of your activity.
* The length of your activity 
    * 1 hour only
    * 1 hour with follow-on course
    * 2-6 hours (can be multiple lessons)
* A list of accessibility accommodations, if your activity has them. These include screen reader compatibility, text-to-speech capabilities, use of high-contrast colors, or any other accommodations made for learners with disabilities.

#### További dolgok, amire szükséged van az órai tervek benyújtásakor

* Link az óratervedhez. Ez lehet weboldal, dropbox link, google meghajtó vagy hasonló szolgáltatás.
* Link az óratervedhez. Ez lehet weboldal, dropbox link, google meghajtó vagy hasonló szolgáltatás. Esetleg semmi?)

#### További dolgok, amelyekre szükséged van online tevékenységek benyújtásakor

* Szervezeted neve és logója
* megfelel-e a COPPA-nak vagy sem
* uRL link felhasználható tanári jegyzetekhez (opcionális)
* A tesztelt / kompatibilis platformok listája: 
    * Web alapú: mely platformokon tesztelt? 
        * OS - Mac, Win vagy egyéb verziók
        * Böngészők - IE11, Edge, Firefox, Chrome, Safari
        * iOS mobil Safari (mobilra optimalizált)
        * Android Chrome (mobilra optimalizált)
    * Nem web alapú: add meg a forrás kód platformját (Mac, Win, iOS, Android, xBox, egyéb)
    * Kihúzva
* A Kódolás órája tevékenység képernyőképe vagy marketing képe. Kérlek, küldj legalább egy képet 4:3 méretben. Legyen legalább 520x390 képpont méretű. Amennyiben nem áll rendelkezésre megfelelő kép, előfordulhat, hogy mi készítünk saját képernyőképet az oktatóanyagból, VAGY dönthetünk úgy is, hogy nem soroljuk fel az oktatóanyagot a listában. Az összes képet URL-linkként szükséges elküldeni a .jpg, .jpeg vagy .png fájlhoz. Az összes képet URL-linkként szükséges elküldeni a .jpg, .jpeg vagy .png fájlhoz.
* A részvétel pontosabb nyomon követése érdekében a harmadik fél partnereinek 1 kocka méretű nyomkövető képet kell alkalmaznia a Kódolás órája gyakorlataik első oldalán. Helyezd el a pixelt a kezdőlapra.
* A tevékenység befejezése után a felhasználókat a [ code.org/api/hour/finish ](https://code.org/api/hour/finish) webhelyre szükséges irányítani, ahol képesek lesznek: 
    * Megosztani a hírt a közösségi médiában, hogy befejezték a Kódolás órája gyakorlatot
    * Igazolást kapni arról, hogy teljesítették az egy órás kódot
    * Megtekinteni a ranglistákat arról, hogy mely országok / városok vesznek részt legeredményesebben a Kódolás órája tevékenységekben
    * Azon felhasználók számára, akik bár egy órát eltöltenek a tevékenység megoldásával, de be nem fejezik azt, kérlek, tégy be egy gombot, amelynek felirata az alábbi: „Befejeztem a Kódolás óráját”, amely visszirányítja őket a <a href = "https: // kódra mutat..org / api / hour / finish "> code.org/api/hour/finish </a> oldalra.
* (Opcionális) Az online felmérés / űrlap hivatkozással nyomon követhetjük a tevékenységekkel kapcsolatos mutatók eredményeit a decemberi esemény hetén. 
    * Online tevékenységekhez (különösen okostelefon- / táblagép-alkalmazásokhoz): 
        * Felhasználók száma
        * Mennyien tejesítették a feladatot
        * A feladattal eltöltött átlagos idő
        * Az összes felhasználó által írt kódsor összesen
        * Hány folytatták tovább a tanulást (bármelyik olyan felhasználó, aki befejezte a feladatot és további feladatokat végez a webhelyeden)
    * Offline tevékenységekhez 
        * A tevékenység papíralapú letöltésének száma (ha van)

#### További dolgok, amelyekre szükséged van a robotikai feladatok benyújtásakor

* Ha robotikához kapcsolódó tevékenységet nyújtassz be, akkor tudnunk kell, hogy az egy diákra eső költségeket
* A robotikához kapcsolódó tevékenységek weboldalon történő megjelenítéséhez szükségünk lesz arra, hogy mintakészleteket küldj az értékelőknek.

[**Vissza az elejére**](#top)

<a id="inclusion"></a>

## Hogyan lesznek értékelve az ötletek

A számítástechnikai oktatók bizottsága minőségi és mennyiségi kritériumok alapján rangsorolja a benyújtott tevékenységeket. Minden tevékenységet felsorolunk, amely megfelel az alapkritériumoknak.

For traditional coding activities, a diverse committee of computer science educators will rank submissions based on qualitative and quantitative criteria. All activities that fit the basic criteria will be listed. Teachers will be able to filter and sort to find the best activities for their classroom.

Amennyiben a felülvizsgálati bizottság a tevékenységet feltöltésre érdemtelennek ítéli minőségbeli okok (hibás kódok, utasítások, melyek megnehezítik a felhasználást), alulreprezentált csoportokat érintő hátrányos megkülönböztetés (rasszista / szexista tartalom), hiányos oktatási értékek (nem tanít számítástechnikai fogalmakat), netán éppen a túlságosan is a szórakoztatást előtérbe helyező hangvétele miatt (elvonja a hallgatók figyelmét, akadályozza a munkát), a tevékenység nem kerül közzétételre.

- Magas minőség
- Az összes demográfiai csoport tanulásának ösztönzése (különösen az alulreprezentált csoportok)
- Oktathatóság (megfelelő módon képes tanítani a számítástechnika fogalmait)
- Szórakoztató és lebilincselő
- Arra ösztönzi a diákokat, hogy készítsenek valami egyedülállót, amelyet megoszthatnak másokkal is (fiatalabb hallgatók: szülőkkel, osztálytársakkal, idősebb hallgatók: az interneten)

Mindezeken túl minden tevékenységnek meg kell felelnie az alábbi feltételeknek ahhoz, hogy elfogadásra kerüljön:

Az új tanároknak és diákoknak szóló önirányított tevékenységek esetében a bírálóbizottság a következőket vizsgálja:

- Megfelelőnek kell lennie egy állami iskolai tanórán való megtartáshoz (fegyveres, erőszakos, szexuális és vallásos tartalom tilos)
- Nincs szükség regisztrációra
- Nem igényel fizetést (kivétel a robotika tevékenységekhez, amelyek robot / kiegészítő vásárlást igényelhetnek) A felülvizsgálati bizottság az önálló munka során feldolgozandó tevékenységek esetében megvizsgálja, hogy:

For self-directed activities for new teachers and students the review committee will be looking for whether:

- Megkapták a legmagasabb értékelést a felülvizsgálati bizottságtól
- Egyórás önálló munkára épülő tevékenységeket céloznak kezdőknek (diákoknak és tanároknak egyaránt)
- Alkalmazható a felhasználók széles köre számára (platform, nyelv és életkor alapján is)

Teachers and students will be able to search through and filter our list of activities based on filters such as grade, experience level, subject, hardware, etc. By default, we will show lesson plans and activities first that:

- Mely platformon és böngészőn működik a legjobban az oktatóprogram?
- Kritikai szempontok - Mit lehetne javítani?
- Alkalmazható a felhasználók széles köre számára (platform, nyelv és életkor alapján is)
- Arra ösztönzi ez a tevékenység a diákokat, hogy valami egyedi dolgot alkossanak?
- Az idei évben kerültek benyújtásra 

[**Vissza az elejére**](#top)

<a id="design"></a>

## Javaslatok egyórás önálló gyakorlatok megtervezéséhez

Activities do not have to include AI, though we are looking to offer more AI related activities.

**Tanári jegyzetek.** A legtöbb tevékenységet a diákoknak önállóan kell tudniuk elvégezni, de ha a tevékenység elvégzéséhez egy tanár segítségére vagy irányítására van szükség, kérjük, adj világos és egyszerű utasításokat tanári jegyzetek formájában egy URL-címen, és nyújtsd be a tevékenységgel együtt. Nemcsak a diákok, de a tanárok egy része is kezdő. Adj meg olyan információkat, mint például:

** Ösztönözd a diákokat arra, hogy osszák meg munkájukat a közösségi médiában. ** Például: „Egyórás kódot csináltam ________-kel? #HourOfCode” vagy "#HourOfCode-ot csináltam a #CSEdWeek részeként.

Minden október 1-je előtt beküldött tevékenység normál áttekintésben részesül, függetlenül attól, hogy részt vesz-e a korai felülvizsgálatban. A szokásos felülvizsgálati időszakban (szeptember-október) a bírálók a tevékenységek értékelésére összpontosítanak a fenti "Hogyan értékeljük a tevékenységeket" részben ismertetett kritériumok szerint. Hacsak te nem szeretnél változtatni a tevékenységen, a véleményezők a korai felülvizsgálati visszajelzésekre fognak támaszkodni a tevékenységek értékelésénél.

- Mely platformon és böngészőn működik a legjobban az oktatóprogram?
- Működik-e okostelefonokon? Tableten?
- Szervezz Te egy rendezvény vagy kérd meg alkalmazottaidat, hogy segítsenek a rendezvények népszerűsítésében a helyi iskolákban, közösségi csoportokban.
- Az osztályteremben történő használat szempontjai? Például, ha vannak videók, javasold a tanárokat, hogy mutassák meg a őket az egész osztálynak

**Incorporate feedback at the end of the activity.** (E.g. “You finished 10 levels and learned about loops! Great job!”)

**Encourage students to post to social media (where appropriate) when they've finished.** For example “I’ve done an Hour of Code with ________ Have you? #HourOfCode” or “I’ve done an #HourOfCode as a part of #CSEdWeek. Have you? @Scratch.” Use the hashtag **#HourOfCode** (with capital letters H, O, C)

**Create your activity in Spanish or in other languages besides English.**

**Explain or connect the activity to a socially significant context.** Computer programming becomes a superpower when students see how it can change the world for the better!

**Make sure your tutorial can be used in a [Pair Programming](http://www.ncwit.org/resources/pair-programming-box-power-collaborative-learning) paradigm.** This is particularly useful for the Hour of Code because many classrooms do not have 1:1 hardware for all students.

[**Vissza az elejére**](#top)

<a id="tm"></a>

## Védjegyre vonatkozó irányelvek

Hour of Code® and Hora del Código® are registered trademarks of Code.org. Many of our tutorial partners have used our "Hour of Code" trademarks on their web sites in connection with their Hour of Code activities. We don't want to prevent this usage, but we want to make sure the usage falls within a few limits:

1. A "Kódolás órája" kifejezést kizárólag a Kódolás órája kampány keretében végzett, nem kereskedelmi célú számítástechnikai oktatással kapcsolatban szabad használni, és minden más célra tilos.
2. A Kódolás órájára való hivatkozás nem sugallja azt, hogy ez a saját márkaneved, annak a Kódolás órájára, egy alulról jövő kezdeményezésre kell utalni. Jó példa: "Vegyen részt a Kódolás órája ® programban az ACMECorp.com oldalon". Helytelen példa: "Próbáld ki a Kódolás óráját a ACME Corp. segítségével.
3. Használd az "®" jelet a "Kódolás órája" legfontosabb helyein, a weboldalán, az alkalmazások leírásában.
4. Helyezd el az idegen nyelvű leírásokat az oldal aján (vagy a láblécben), továbbá a Kódolás órája, a CSEdWeek és a Code.org weboldalakra mutató linkeket is az alábbi szöveggel együtt: a) A Kódolás órája® és Hora del Código® a Code.org bejegyzett védjegyei; és b) "A [Kódolás órája](http://hourofcode.com/) ® a [Code.org](http://code.org/) által indított országos kezdeményezés, amelynek célja, hogy diákok milliói kapjanak egy órányi informatikát és a számítógépes programozást."
5. Ne használd a Kódolás órája kifejezést az applikációk nevében.
6. Ne használd a "Kódolás órája" kifejezést kereskedelmi célra vagy kereskedelemmel kapcsolatban (pl. a Kódolás órája elhelyezése egy fizetőfal mögé; más fizetős szolgáltatás népszerűsítése a Kódolás órája részeként; Kódolás órájával kapcsolatos termékek értékesítése).
7. Ne használd a Kódolás óráját olyan tevékenységgel kapcsolatban, amely bejelentkezést vagy fiók létrehozását igényli.

[**Vissza az elejére**](#top)

<a id="pixel"></a>

## Pixel követése

Ha a gyakorlatot jóváhagyják, és az felkerül a végleges gyakorlatok oldalára, a Code.org ad egy egyedi nyomkövető pixelt, amelyet be lehet építeni a saját gyakorlatba. Lásd az alábbi példát. Do not include on any interim pages of your tutorial.

This will allow us to count users who do your Hour of Code tutorial. It will lead to more accurate participation counts for your tutorial.

If your tutorial is approved and included on the final tutorial page, Code.org will provide you with a unique tracking pixel for you to integrate into your tutorial. See example below.

IMG SRC = <http://code.org/api/hour/begin_dance.png>

Example tracking pixels for Dance Party:

IMG SRC = <http://code.org/api/hour/begin_dance.png>   


[**Vissza az elejére**](#top)

<a id="promote"></a>

## Tevékenységeid promotálása, a CSEdWeek és a Kódolás órája

Please promote your activity to your network! Direct them to your Hour of Code page. Your users are much more likely to react to a mailing from you about your activity. Use the international Hour of Code campaign during Computer Science Education Week as an excuse to encourage users to invite others to join in, and help us reach more students!

- Ajánld a Kódolás óráját és CSEdWeek-et a weboldaladon. Pl.: <http://www.tynker.com/hour-of-code>
- Reklámozd a Kódolás óráját a közösségi médiában, a hagyományos médiában, levelezőlistákon stb. használva a #HourOfCode hashtag-et (nagybetűkkel H, O, C)
- Szervezz Te egy rendezvény vagy kérd meg alkalmazottaidat, hogy segítsenek a rendezvények népszerűsítésében a helyi iskolákban, közösségi csoportokban.
- További információért olvasd el a [erőforráskészletünket](https://hourofcode.com/us/promote).

[**Vissza az elejére**](#top)

{{ signup_button }}