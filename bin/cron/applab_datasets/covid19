#!/usr/bin/env ruby

# TODO: unfirebase, also update datablock storage: #56993
# TODO: post-firebase-cleanup, remove the firebase version: #56994

# This script fetches covid19 data and uploads to the shared firebase channel.
require_relative '../../../deployment'
require 'cdo/only_one'
require 'net/http'
require 'csv'
require 'date'
require 'datapackage'

# TODO: post-firebase-cleanup, remove the firebase reference: #56994
require_relative '../../../dashboard/legacy/middleware/helpers/firebase_helper'

US_DATA_URL = "https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-states.csv"
MAX_DAYS = 90 # there are 55 states/territories, and the table must be less than 5000 total rows (5000/55 = 90.9)
MAX_WEEKS = 26 # there are 188 countries, and the table must be less than 5000 total rows (5000/188 = 26.5)

def get_us_covid19_data
  response = Net::HTTP.get_response(URI(US_DATA_URL))
  return unless response.code == '200'
  response.body.force_encoding('utf-8')
  lines = CSV.parse(response.body, headers: true)

  # Group records by date
  daily_data = {}
  lines.each do |line|
    record = {}
    parsed_date = Date.strptime(line.field("date"), "%Y-%m-%d")
    formatted_date = parsed_date.strftime('%a %-m/%-d') # ex Wed 1/7
    record['Date'] = formatted_date
    record['State'] = line.field("state")
    record['Total Confirmed Cases'] = line.field("cases").to_i
    record['Total Deaths'] = line.field("deaths").to_i
    daily_data[formatted_date] ||= []
    daily_data[formatted_date].push record
  end

  # Truncate to MAX_DAYS most recent days
  days = if daily_data.length > MAX_DAYS
           daily_data.keys.slice(daily_data.length - MAX_DAYS, MAX_DAYS)
         else
           daily_data.keys
         end

  # Add id to each record
  columns = ['id', 'Date', 'State', 'Total Confirmed Cases', 'Total Deaths']
  records = {}
  id = 1
  days.each do |day|
    day_records = daily_data[day]
    day_records.each do |record|
      record['id'] = id
      records[id] = record.to_json
      id += 1
    end
  end

  return records, columns
end

def main
  # TODO: post-firebase-cleanup, remove the firebase reference: #56994
  fb = FirebaseHelper.new('shared')
  records, columns = get_us_covid19_data
  fb.upload_live_table('COVID-19 Cases per US State', records, columns)
end

main if only_one_running?(__FILE__)
